Imports System.Diagnostics
Imports System.Text
Imports System.IO

Public Class frmEstado
    Private WithEvents tmrCrono As Timers.Timer

    Private Sub SetUpTimer()
        Try
            tmrCrono = New Timers.Timer()
            With tmrCrono
                .AutoReset = True
                .Interval = 1000
                .Start()
            End With
        Catch obEx As Exception
            LogToFile("frmEstado.SetupTimer.Error: " & obEx.Message)
        End Try
    End Sub

    Private Sub SetDownTimer()
        If tmrCrono IsNot Nothing Then
            tmrCrono.Stop()
            tmrCrono.Dispose()
        End If
    End Sub

    Public Sub RefreshBandejaDeEntrada()
        'CheckForIllegalCrossThreadCalls = False
        If mdlIEFEMain.AppConfig.DoNotUpdateUI Then Exit Sub
        Try
            If mdlIEFEMain.AppConfig.InPath = vbNullString Then
            ElseIf Directory.Exists(mdlIEFEMain.AppConfig.InPath) = True Then
                Dim diiPath As New DirectoryInfo(mdlIEFEMain.AppConfig.InPath)
                Dim fiiList As FileInfo() = diiPath.GetFiles()
                Me.lvwInFiles.Items.Clear()
                For Each fi As FileInfo In fiiList
                    Me.lvwInFiles.Items.Add(fi.Name.ToUpper.Replace(".TXT", ""))
                Next fi
            End If
        Catch ex As Exception
            LogToFile("RefreshBandejaDeEntrada.Error: " & ex.Message)
        End Try
    End Sub

    Public Sub RefreshErrorList()
        Try
            If mdlUniversal.ErrorList.Count <> Me.lvwErr.Items.Count Then
                Me.lvwErr.Items.Clear()
                For Each strItem As String In mdlUniversal.ErrorList
                    Me.lvwErr.Items.Add(strItem)
                Next
            End If
        Catch ex As System.Exception
            LogToFile("InsertErrorItem.Error: " & ex.Message)
        End Try
    End Sub

    Public Sub ClearErrorList()
        Me.lvwErr.Items.Clear()
    End Sub

    Public Sub RefreshBandejaDeSalida()
        'CheckForIllegalCrossThreadCalls = False
        If mdlIEFEMain.AppConfig.DoNotUpdateUI Then Exit Sub
        Try
            If mdlIEFEMain.AppConfig.InPath = vbNullString Then
            ElseIf Directory.Exists(mdlIEFEMain.AppConfig.OutPath) = True Then
                Dim diiPath As New DirectoryInfo(mdlIEFEMain.AppConfig.OutPath)
                Dim fiiList As FileInfo() = diiPath.GetFiles()
                Me.lvwOutFiles.Items.Clear()
                For Each fi As FileInfo In fiiList
                    Me.lvwOutFiles.Items.Add(fi.Name.ToUpper.Replace(".TXT", ""))
                Next fi
            End If
        Catch ex As Exception
            LogToFile("RefreshBandejaDeSalida.Error: " & ex.Message)
        End Try
    End Sub

    Public Sub tmrCrono_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmrCrono.Elapsed
        Try
            Dim intSeg As Long = 0
            If mdlIEFEMain.mobTimer.Enabled Then
                intSeg = DateDiff(DateInterval.Second, Now, mdlIEFEMain.NextTimeout)
                Me.tsStatus.Text = "Reintento programado en " & Format$(Int(intSeg \ 60), "00") & ":" & Format(Int((intSeg / 60 - intSeg \ 60) * 60), "00")
            ElseIf Me.tsStatus.Text <> "" Then
                Me.tsStatus.Text = ""
            End If
            intSeg = DateDiff(DateInterval.Second, Now, mdlIEFEMain.NextCheckTime)
            If mdlIEFEMain.ProcesandoLista Then
                Me.tsCheck.Text = "CHECK"
            ElseIf intSeg = 0 And Not mdlIEFEMain.mobTimer.Enabled Then
                Me.tsCheck.Text = "check"
            Else
                Me.tsCheck.Text = Format$(Int(intSeg \ 60), "00") & ":" & Format(Int((intSeg / 60 - intSeg \ 60) * 60), "00")
            End If

            RefreshErrorList()

        Catch obEx As Exception
            LogToFile("frmEstado.Timer.Error: " & obEx.Message)
        End Try
    End Sub

    Private Sub frmEstado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "Visor de estados - Facturación Electrónica (WSFEv1) V." & mdlGetVersion() & " - OSAM"
        CheckForIllegalCrossThreadCalls = False
        SetUpTimer()
    End Sub

    Private Sub frmEstado_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        SetDownTimer()
    End Sub

    Private Sub cmdClearErr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClearErr.Click
        mdlUniversal.ErrorList.Clear()
    End Sub
End Class