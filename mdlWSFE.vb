Imports System.Xml
Imports System.Xml.Serialization
Imports System.Xml.XmlException
Imports System.IO
Imports Microsoft.VisualBasic.FileIO.FileSystem

Public Module mdlWSFE
    Private objWSFE As New wsfe.Service
    Private objFEAuthRequest As New wsfe.FEAuthRequest
    Private RutaTicketAcceso As String = My.Application.Info.DirectoryPath & "\WSAA_TA.xml" ' Debe indicar la Ruta de su Ticket de acceso
    Private CUIT As String               ' Debe indicar la CUIT del emisor de los comprobantes
    Private Token As String
    Private Sign As String
    Private CAE As String

    Public Function Inicializado() As Boolean
        If CUIT = vbNullString Or Token = vbNullString Or Sign = vbNullString Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function Inicializar(ByVal strCUIT As String) As Boolean
        objWSFE.Url = "https://servicios1.afip.gov.ar/wsfe/service.asmx?WSDL"
        'objWSFE.Url = "https://wswhomo.afip.gov.ar/wsfe/service.asmx?WSDL"
        objWSFE.Timeout = 60 * 2 * 1000
        CUIT = strCUIT
        ' Obtengo del tikcet de acceso los campos token y sign
        If FileExists(RutaTicketAcceso) Then
            Dim m_xmld As XmlDocument
            Dim m_nodelist As XmlNodeList
            Dim m_node As XmlNode
            m_xmld = New XmlDocument()
            m_xmld.Load(RutaTicketAcceso)
            m_nodelist = m_xmld.SelectNodes("/loginTicketResponse/credentials")
            For Each m_node In m_nodelist
                Token = m_node.ChildNodes.Item(0).InnerText
                Sign = m_node.ChildNodes.Item(1).InnerText
            Next
            CAE = vbNullString
            Return True
        Else
            LogToFile("Error en mdlWSFE.Inicializar: No se encontr� el ticket de acceso (Archivo: " + RutaTicketAcceso)
        End If
    End Function

    Public Function SolicitarCAE( _
        ByVal FileName As String, _
        ByVal NumeroDesde As Integer, _
        ByVal NumeroHasta As Integer, _
        ByVal EsServicio As Boolean, _
        ByVal TipoDoc As Integer, _
        ByVal NroDoc As String, _
        ByVal TipoComprobante As Integer, _
        ByVal PuntoDeVenta As Integer, _
        ByVal ImporteTotal As Decimal, _
        ByVal ImporteNoGravado As Decimal, _
        ByVal ImporteNeto As Decimal, _
        ByVal ImporteIVA As Decimal, _
        ByVal ImporteRNI As Decimal, _
        ByVal ImporteEXE As Decimal, _
        ByVal FechaComprobante As Date, _
        ByVal FechaServDesde As Date, _
        ByVal FechaServHasta As Date, _
        ByVal FechaVencimiento As Date, _
        ByRef outCAE As String, _
        ByRef outVencimiento As String, _
        ByRef outReferencia As String, _
        ByRef outErrorDescription As String _
    ) As Boolean

        Dim objFERequest As New wsfe.FERequest
        Dim objFECabeceraRequest As New wsfe.FECabeceraRequest
        Dim ArrayOfFEDetalleRequest(0) As wsfe.FEDetalleRequest
        Dim objFEResponse As New wsfe.FEResponse

        If mdlWSFE.CUIT = vbNullString Then
            LogToFile("mdlWSFE.SolicitarCAE() CUIT no inicializado.")
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                                  "Archivo:" & FileName & vbCrLf & _
                                  "Detalle del Error: CUIT no inicializado."
            Return False
        End If
        If mdlWSFE.Token = vbNullString Then
            LogToFile("mdlWSFE.SolicitarCAE() Token no inicializado.")
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                                  "Archivo:" & FileName & vbCrLf & _
                                  "Detalle del Error: Token no inicializado."
            Return False
        End If
        If mdlWSFE.Sign = vbNullString Then
            LogToFile("mdlWSFE.SolicitarCAE() Sign no inicializado.")
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                                  "Archivo:" & FileName & vbCrLf & _
                                  "Detalle del Error: Sign no inicializado."
            Return False
        End If

        objFEAuthRequest.cuit = mdlWSFE.CUIT 'validar
        objFEAuthRequest.Token = mdlWSFE.Token 'validar
        objFEAuthRequest.Sign = mdlWSFE.Sign 'validar

        Dim intId As Integer
        Dim strErrorDescription As String = ""
        If ObtenerProximoID(intId, strErrorDescription, FileName) = False Then
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                                  "Archivo:" & FileName & vbCrLf & _
                                  "Detalle del Error: Error al obtener proximo Id, " & vbCrLf & _
                                  strErrorDescription
            Return False
        End If
        objFECabeceraRequest.id = intId
        objFECabeceraRequest.cantidadreg = 1
        objFECabeceraRequest.presta_serv = IIf(EsServicio, 1, 0)
        objFERequest.Fecr = objFECabeceraRequest

        outCAE = ""
        outVencimiento = ""
        outReferencia = ""

        'Detalles del comprobante
        ArrayOfFEDetalleRequest(0) = New wsfe.FEDetalleRequest
        With ArrayOfFEDetalleRequest(0) 'validar todo
            .tipo_doc = TipoDoc
            .nro_doc = NroDoc
            .tipo_cbte = TipoComprobante
            .punto_vta = PuntoDeVenta
            .cbt_desde = NumeroDesde
            .cbt_hasta = NumeroHasta
            .imp_total = ImporteTotal
            .imp_tot_conc = ImporteNoGravado
            .imp_neto = ImporteNeto
            .impto_liq = ImporteIVA
            .impto_liq_rni = ImporteRNI
            .imp_op_ex = ImporteEXE
            .fecha_cbte = Format$(FechaComprobante, "yyyyMMdd")
            .fecha_serv_desde = Format$(FechaServDesde, "yyyyMMdd")
            .fecha_serv_hasta = Format$(FechaServHasta, "yyyyMMdd")
            .fecha_venc_pago = Format$(FechaVencimiento, "yyyyMMdd")
        End With
        objFERequest.Fedr = ArrayOfFEDetalleRequest

        ' Invoco al m�todo FEAutRequest
        ' Obtengo los XML de request/response y los escribo en el disco
        Dim intSufijo As Integer = 1
        Do Until File.Exists(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FERequest" & Format$(intSufijo, "00000000") & ".xml") = False
            intSufijo += 1
        Loop
        Try
            Dim file1 As New StreamWriter(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FERequest" & Format$(intSufijo, "00000000") & ".xml")
            Dim writer1 As New XmlSerializer(GetType(wsfe.FERequest))
            writer1.Serialize(file1, objFERequest)
            file1.Close()

            LogToFile(Now & " Call FEAutRequest (id:" & objFERequest.Fecr.id & ")")
            objFEResponse = objWSFE.FEAutRequest(objFEAuthRequest, objFERequest)
            LogToFile(Now & " Return from FEAutRequest (id:" & objFERequest.Fecr.id & ")")
            If objFEResponse.FecResp Is Nothing Then
                LogToFile("FEResponse.RError.percode: " + objFEResponse.RError.percode.ToString + vbCrLf + _
                          "FEResponse.RError.perrmsg: " + objFEResponse.RError.perrmsg)
                outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                                      "Archivo:" & FileName & vbCrLf & _
                                      "Detalle del Error: AFIP detect� el siguiente error: " & vbCrLf & _
                                      objFEResponse.RError.perrmsg
            Else
                LogToFile("FEResponse.FecResp.motivo: " + objFEResponse.FecResp.motivo + vbCrLf + _
                "FEResponse.FecResp.reproceso: " + objFEResponse.FecResp.reproceso + vbCrLf + _
                "FEResponse.FecResp.resultado: " + objFEResponse.FecResp.resultado)
                LogToFile("FEResponse.RError.percode: " & objFEResponse.RError.percode.ToString & vbCrLf & _
                          "FEResponse.RError.perrmsg: " & objFEResponse.RError.perrmsg)
                'For d = 0 To (indicemax)
                LogToFile("FEResponse.FedResp(" & 0 & ").cae: " & objFEResponse.FedResp(0).cae.ToString & vbCrLf & _
                          "FEResponse.FedResp(" & 0 & ").resultado: " & objFEResponse.FedResp(0).resultado)
                If objFEResponse.FedResp(0).resultado = "R" Then
                    Dim strMotivo As String
                    Dim intI As Integer
                    Dim strMotivos() = objFEResponse.FedResp(0).motivo.Split(";")
                    outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                                          "Archivo:" & FileName & vbCrLf & _
                                          "Detalle del Error: Solicitud rechazada por AFIP." & vbCrLf & _
                                          "Motivos del Rechazo: " & vbCrLf
                    For intI = 0 To strMotivos.Length - 1
                        strMotivo = strMotivos(intI)
                        LogToFile("FEResponse.FedResp(" & 0 & ").motivos: " & DescripcionMotivo(strMotivos(intI)))
                        outErrorDescription = outErrorDescription & DescripcionMotivo(strMotivos(intI)) & vbCrLf
                        If strMotivos(intI) = "11" Then
                            Dim intNextNum As Integer = mdlWSFE.ObtenerProximoNumeroComprobante(ArrayOfFEDetalleRequest(0).tipo_cbte, ArrayOfFEDetalleRequest(0).punto_vta, FileName)
                            LogToFile("HINT: el proximo numero esperado para el comprobante tipo " & ArrayOfFEDetalleRequest(0).tipo_cbte & _
                            "y punto de venta " & ArrayOfFEDetalleRequest(0).punto_vta & _
                            " es " & intNextNum)
                            outErrorDescription = outErrorDescription & "Se envi� el numero " & _
                                Format$(ArrayOfFEDetalleRequest(0).punto_vta, "0000") & "-" & _
                                Format$(ArrayOfFEDetalleRequest(0).cbt_hasta, "00000000") & vbCrLf
                            outErrorDescription = outErrorDescription & "Se esperaba el numero " & _
                                Format$(ArrayOfFEDetalleRequest(0).punto_vta, "0000") & "-" & _
                                Format$(intNextNum, "00000000") & vbCrLf
                        End If
                    Next
                    outCAE = ""
                    outVencimiento = ""
                    outReferencia = ""
                Else
                    outCAE = objFEResponse.FedResp(0).cae.ToString & ""
                    outVencimiento = objFEResponse.FedResp(0).fecha_vto & "" '01/01/2001
                    outReferencia = Format$(objFEResponse.FedResp(0).tipo_cbte, "00") & "-" & _
                                    Format$(objFEResponse.FedResp(0).punto_vta, "0000") & "-" & _
                                    Format$(objFEResponse.FedResp(0).cbt_hasta, "00000000")
                    SolicitarCAE = True
                End If
                'Next d
            End If
        Catch ex As TimeoutException
            LogToFile("Error en mdlWSFE.ObtenerProximoID: " & ex.Message)
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                               "El servidor de AFIP no ha respondido en el tiempo esperado." & vbCrLf & _
                               "Consultar en Mesa ayuda sri@afip.gov.ar o 0800-333-6372 opci�n 1 si el servidor de AFIP se encuentra en l�nea."
            SolicitarCAE = False
        Catch ex As Exception
            LogToFile(Now & vbCrLf & "Error en mdlWSFE.SolicitarCAE: " & ex.Message)
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                                  "Archivo:" & FileName & vbCrLf & _
                                  "Detalle del Error: " & vbCrLf & _
                                  ex.Message
            SolicitarCAE = False
        End Try

        Dim writer2 As New XmlSerializer(GetType(wsfe.FEResponse))
        Dim file2 As New StreamWriter(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FEResponse" & Format$(intSufijo, "00000000") & ".xml")
        writer2.Serialize(file2, objFEResponse)
        file2.Close()

    End Function

    Public Function SolicitarCAEManual( _
        ByVal Id As String, _
        ByRef EsServicio As Boolean, _
        ByRef NumeroDesde As Integer, _
        ByRef NumeroHasta As Integer, _
        ByRef TipoDoc As Integer, _
        ByRef NroDoc As String, _
        ByRef TipoComprobante As Integer, _
        ByRef PuntoDeVenta As Integer, _
        ByRef ImporteTotal As Decimal, _
        ByRef ImporteNoGravado As Decimal, _
        ByRef ImporteNeto As Decimal, _
        ByRef ImporteIVA As Decimal, _
        ByRef ImporteRNI As Decimal, _
        ByRef ImporteEXE As Decimal, _
        ByRef FechaComprobante As Date, _
        ByRef FechaServDesde As Date, _
        ByRef FechaServHasta As Date, _
        ByRef FechaVencimiento As Date, _
        ByRef outCAE As String, _
        ByRef outVencimiento As String, _
        ByRef outErrorDescription As String _
        ) As String

        Dim objFERequest As New wsfe.FERequest
        Dim objFECabeceraRequest As New wsfe.FECabeceraRequest
        Dim ArrayOfFEDetalleRequest(0) As wsfe.FEDetalleRequest
        Dim objFEResponse As New wsfe.FEResponse
        Dim strErrorDescription As String = ""

        If Inicializado() = False Then
            If AutenticacionWSAA.ObtenerAutorizacion(strErrorDescription) = True Then
                Inicializar(AppConfig.CUIT) 'Para refrescar el Token y Sign
                If Inicializado() = False Then
                    LogToFile(Now & " Token y Sign no inicializados... seguramente habra un error en ObtenerProximoNumeroComprobante")
                End If
            Else
                LogToFile(Now & strErrorDescription)
            End If
        End If

        objFEAuthRequest.cuit = mdlWSFE.CUIT 'validar
        objFEAuthRequest.Token = mdlWSFE.Token 'validar
        objFEAuthRequest.Sign = mdlWSFE.Sign 'validar

        objFECabeceraRequest.id = Id
        objFECabeceraRequest.cantidadreg = 1
        objFECabeceraRequest.presta_serv = IIf(EsServicio, 1, 0)
        objFERequest.Fecr = objFECabeceraRequest

        outCAE = ""
        outVencimiento = ""

        'Detalles del comprobante
        ArrayOfFEDetalleRequest(0) = New wsfe.FEDetalleRequest
        With ArrayOfFEDetalleRequest(0) 'validar todo
            .tipo_doc = TipoDoc
            .nro_doc = NroDoc
            .tipo_cbte = TipoComprobante
            .punto_vta = PuntoDeVenta
            .cbt_desde = NumeroDesde
            .cbt_hasta = NumeroHasta
            .imp_total = ImporteTotal
            .imp_tot_conc = ImporteNoGravado
            .imp_neto = ImporteNeto
            .impto_liq = ImporteIVA
            .impto_liq_rni = ImporteRNI
            .imp_op_ex = ImporteEXE
            .fecha_cbte = Format$(FechaComprobante, "yyyyMMdd")
            .fecha_serv_desde = Format$(FechaServDesde, "yyyyMMdd")
            .fecha_serv_hasta = Format$(FechaServHasta, "yyyyMMdd")
            .fecha_venc_pago = Format$(FechaVencimiento, "yyyyMMdd")
        End With
        objFERequest.Fedr = ArrayOfFEDetalleRequest

        ' Invoco al m�todo FEAutRequest
        ' Obtengo los XML de request/response y los escribo en el disco
        Dim intSufijo As Integer = 1
        Do Until File.Exists(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FERequest" & Format$(intSufijo, "00000000") & ".xml") = False
            intSufijo += 1
        Loop
        Try
            Dim file1 As New StreamWriter(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FERequest" & Format$(intSufijo, "00000000") & ".xml")
            Dim writer1 As New XmlSerializer(GetType(wsfe.FERequest))
            writer1.Serialize(file1, objFERequest)
            file1.Close()

            LogToFile(Now & " Call FEAutRequest (id:" & objFERequest.Fecr.id & ")")
            objFEResponse = objWSFE.FEAutRequest(objFEAuthRequest, objFERequest)
            LogToFile(Now & " Return from FEAutRequest (id:" & objFERequest.Fecr.id & ")")
            If objFEResponse.FecResp Is Nothing Then
                LogToFile("FEResponse.RError.percode: " + objFEResponse.RError.percode.ToString + vbCrLf + _
                          "FEResponse.RError.perrmsg: " + objFEResponse.RError.perrmsg)
                outErrorDescription = "Error al Solicitar CAE Manual en AFIP." & vbCrLf & _
                                      "Id: " & Id & vbCrLf & _
                                      "Detalle del Error: AFIP detect� el siguiente error: " & vbCrLf & _
                                      objFEResponse.RError.perrmsg
            Else
                LogToFile("FEResponse.FecResp.motivo: " + objFEResponse.FecResp.motivo + vbCrLf + _
                "FEResponse.FecResp.reproceso: " + objFEResponse.FecResp.reproceso + vbCrLf + _
                "FEResponse.FecResp.resultado: " + objFEResponse.FecResp.resultado)
                LogToFile("FEResponse.RError.percode: " & objFEResponse.RError.percode.ToString & vbCrLf & _
                          "FEResponse.RError.perrmsg: " & objFEResponse.RError.perrmsg)
                'For d = 0 To (indicemax)
                LogToFile("FEResponse.FedResp(" & 0 & ").cae: " & objFEResponse.FedResp(0).cae.ToString & vbCrLf & _
                          "FEResponse.FedResp(" & 0 & ").resultado: " & objFEResponse.FedResp(0).resultado)
                If objFEResponse.FedResp(0).resultado = "R" Then
                    Dim strMotivo As String
                    Dim intI As Integer
                    Dim strMotivos() = objFEResponse.FedResp(0).motivo.Split(";")
                    outErrorDescription = "Error al Solicitar CAE Manual en AFIP." & vbCrLf & _
                                          "Id: " & Id & vbCrLf & _
                                          "Detalle del Error: Solicitud rechazada por AFIP." & vbCrLf & _
                                          "Motivos del Rechazo: " & vbCrLf
                    For intI = 0 To strMotivos.Length - 1
                        strMotivo = strMotivos(intI)
                        LogToFile("FEResponse.FedResp(" & 0 & ").motivos: " & DescripcionMotivo(strMotivos(intI)))
                        outErrorDescription = outErrorDescription & DescripcionMotivo(strMotivos(intI)) & vbCrLf
                        If strMotivos(intI) = "11" Then
                            Dim intNextNum As Integer = mdlWSFE.ObtenerProximoNumeroComprobante(ArrayOfFEDetalleRequest(0).tipo_cbte, ArrayOfFEDetalleRequest(0).punto_vta, "")
                            LogToFile("HINT: el proximo numero esperado para el comprobante tipo " & ArrayOfFEDetalleRequest(0).tipo_cbte & _
                            "y punto de venta " & ArrayOfFEDetalleRequest(0).punto_vta & _
                            " es " & intNextNum)
                            outErrorDescription = outErrorDescription & "Se envi� el numero " & _
                                Format$(ArrayOfFEDetalleRequest(0).punto_vta, "0000") & "-" & _
                                Format$(ArrayOfFEDetalleRequest(0).cbt_hasta, "00000000") & vbCrLf
                            outErrorDescription = outErrorDescription & "Se esperaba el numero " & _
                                Format$(ArrayOfFEDetalleRequest(0).punto_vta, "0000") & "-" & _
                                Format$(intNextNum, "00000000") & vbCrLf
                        End If
                    Next
                    outCAE = ""
                    outVencimiento = ""
                Else
                    outCAE = objFEResponse.FedResp(0).cae.ToString & ""
                    outVencimiento = objFEResponse.FedResp(0).fecha_vto & "" '01/01/2001
                    EsServicio = IIf(objFEResponse.FecResp.presta_serv = "1", True, False)
                    Try
                        NumeroDesde = objFEResponse.FedResp(0).cbt_desde
                        NumeroHasta = objFEResponse.FedResp(0).cbt_hasta
                        TipoDoc = objFEResponse.FedResp(0).tipo_doc
                        NroDoc = objFEResponse.FedResp(0).nro_doc
                        TipoComprobante = objFEResponse.FedResp(0).tipo_cbte
                        PuntoDeVenta = objFEResponse.FedResp(0).punto_vta
                        ImporteTotal = objFEResponse.FedResp(0).imp_total
                        ImporteNoGravado = objFEResponse.FedResp(0).imp_tot_conc
                        ImporteNeto = objFEResponse.FedResp(0).imp_neto
                        ImporteIVA = objFEResponse.FedResp(0).impto_liq
                        ImporteRNI = objFEResponse.FedResp(0).impto_liq_rni
                        ImporteEXE = objFEResponse.FedResp(0).imp_op_ex
                        FechaComprobante = objFEResponse.FedResp(0).fecha_cbte
                        FechaServDesde = objFEResponse.FedResp(0).fecha_serv_desde
                        FechaServHasta = objFEResponse.FedResp(0).fecha_serv_hasta
                        FechaVencimiento = objFEResponse.FedResp(0).fecha_venc_pago
                    Catch ex As Exception
                    End Try
                End If
                'Next d
            End If
        Catch ex As TimeoutException
            LogToFile("Error en mdlWSFE.ObtenerProximoID: " & ex.Message)
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                               "El servidor de AFIP no ha respondido en el tiempo esperado." & vbCrLf & _
                               "Consultar en Mesa ayuda sri@afip.gov.ar o 0800-333-6372 opci�n 1 si el servidor de AFIP se encuentra en l�nea."
        Catch ex As Exception
            LogToFile(Now & vbCrLf & "Error en mdlWSFE.SolicitarCAE: " & ex.Message)
            outErrorDescription = "Error al Solicitar CAE Manual en AFIP." & vbCrLf & _
                                  "Id: " & Id & vbCrLf & _
                                  "Detalle del Error: " & vbCrLf & _
                                  ex.Message
        End Try

        Dim writer2 As New XmlSerializer(GetType(wsfe.FEResponse))
        Dim file2 As New StreamWriter(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FEResponse" & Format$(intSufijo, "00000000") & ".xml")
        writer2.Serialize(file2, objFEResponse)
        file2.Close()
        Dim srdTxt As StreamReader
        srdTxt = File.OpenText(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FEResponse" & Format$(intSufijo, "00000000") & ".xml")
        SolicitarCAEManual = srdTxt.ReadToEnd()
        srdTxt.Close()
    End Function

    Private Function ObtenerProximoID(ByRef Id As Integer, ByRef ErrorDescription As String, ByVal FileName As String) As Integer
        ' Invoco al m�todo FEUltNroRequest para setear el campo FECabeceraRequest.id del Formulario
        Dim objFEUltNroResponse As New wsfe.FEUltNroResponse
        Id = 0
        objFEAuthRequest.cuit = CUIT
        Try
            Dim i As Integer
            For i = 1 To 10
                objFEAuthRequest.Token = Token
                objFEAuthRequest.Sign = Sign
                LogToFile(Now & " Call FEUltNroRequest")
                objFEUltNroResponse = objWSFE.FEUltNroRequest(objFEAuthRequest)
                LogToFile(Now & " Return from FEUltNroRequest")
                If objFEUltNroResponse.nro Is Nothing Then
                    LogToFile("Error al solicitar el campo [Ultimo Numero de Respuesta] en servicio AFIP" & vbCrLf & _
                              "C�digo: " & objFEUltNroResponse.RError.percode.ToString & vbCrLf & _
                              "Descripci�n: " & objFEUltNroResponse.RError.perrmsg)
                    Select Case objFEUltNroResponse.RError.percode
                        Case 1000
                            'Usuario no autorizado a realizar esta operacion. 
                            'ValidacionDeToken: No validaron las fechas del token GenTime, ExpTime, 
                            Dim strErrorDescription As String = ""
                            If AutenticacionWSAA.ObtenerAutorizacion(strErrorDescription) = True Then
                                Inicializar(CUIT) 'Para refrescar el Token y Sign
                            Else
                                LogToFileAndScreen(strErrorDescription & ". Intento #" & i, FileName)
                            End If
                        Case Else
                            Exit For
                    End Select
                Else
                    Id = objFEUltNroResponse.nro.value + 1
                    Return True
                End If
            Next
            Return False
        Catch ex As TimeoutException
            LogToFile("Error en mdlWSFE.ObtenerProximoID: " & ex.Message)
            ErrorDescription = "Error al Obtener Proximo ID: " & vbCrLf & _
                               "El servidor de AFIP no ha respondido en el tiempo esperado." & vbCrLf & _
                               "Consultar en Mesa ayuda sri@afip.gov.ar o 0800-333-6372 opci�n 1 si el servidor de AFIP se encuentra en l�nea."
            Return False
        Catch ex As Exception
            LogToFile("Error en mdlWSFE.ObtenerProximoID: " & ex.Message)
            ErrorDescription = "Error al Obtener Proximo ID: " & ex.Message
            Return False
        End Try
    End Function

    Public Function ObtenerProximoNumeroComprobante(ByVal TipoCbte As Integer, ByVal PtoVta As Integer, ByVal FileName As String) As Integer
        'Con el m�todo FERecuperaLastCMPRequest puedo determinar el numero de comprobante que
        'debieramos estar procesando en este momento.
        'nos serviria para llenar FEDetalleRequest.cbt_desde y FEDetalleRequest.cbt_hasta

        Dim objFELastCMPtype As New wsfe.FELastCMPtype
        Dim objFERecuperaLastCMPResponse As New wsfe.FERecuperaLastCMPResponse

        If Inicializado() = False Then
            Dim strErrorDescription As String = ""
            If AutenticacionWSAA.ObtenerAutorizacion(strErrorDescription) = True Then
                Inicializar(AppConfig.CUIT) 'Para refrescar el Token y Sign
                If Inicializado() = False Then
                    LogToFile(Now & " Token y Sign no inicializados... seguramente habra un error en ObtenerProximoNumeroComprobante")
                End If
            Else
                LogToFile(Now & strErrorDescription)
            End If
        End If
        objFEAuthRequest.cuit = mdlWSFE.CUIT 'validar
        objFEAuthRequest.Token = mdlWSFE.Token 'validar
        objFEAuthRequest.Sign = mdlWSFE.Sign 'validar

        objFELastCMPtype.TipoCbte = TipoCbte '6
        objFELastCMPtype.PtoVta = PtoVta '1
        Try
            LogToFile(Now & " Call FERecuperaLastCMPRequest")
            objFERecuperaLastCMPResponse = objWSFE.FERecuperaLastCMPRequest(objFEAuthRequest, objFELastCMPtype)
            LogToFile(Now & " Returned from FERecuperaLastCMPRequest")
            If objFERecuperaLastCMPResponse.RError.percode <> 0 Then
                LogToFileAndScreen(Now & " Error logico en ObtenerProximoNumeroComprobante(): (" & objFERecuperaLastCMPResponse.RError.percode & ") " & objFERecuperaLastCMPResponse.RError.perrmsg, FileName)
            End If

            ' Obtengo los XML de request/response y los escribo en el disco
            Dim writer1 As New XmlSerializer(GetType(wsfe.FELastCMPtype))
            Dim intSufijo As Integer = 1
            Do Until File.Exists(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FELastCMPtype" & Format$(intSufijo, "00000000") & ".xml") = False
                intSufijo += 1
            Loop
            Dim file1 As New StreamWriter(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FELastCMPtype" & Format$(intSufijo, "00000000") & ".xml")
            writer1.Serialize(file1, objFELastCMPtype)
            file1.Close()

            Dim writer2 As New XmlSerializer(GetType(wsfe.FERecuperaLastCMPResponse))
            Dim file2 As New StreamWriter(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FERecuperaLastCMPResponse" & Format$(intSufijo, "00000000") & ".xml")
            writer2.Serialize(file2, objFERecuperaLastCMPResponse)
            file2.Close()

            Return objFERecuperaLastCMPResponse.cbte_nro + 1
        Catch ex As TimeoutException
            LogToFile("Error en mdlWSFE.ObtenerProximoID: " & ex.Message)
            Return False
        Catch ex As Exception
            LogToFile("Error en mdlWSFE.ObtenerProximoNumeroComprobante: " & ex.Message)
        End Try
    End Function

    Private Function DescripcionMotivo(ByVal strCodigo As String) As String
        Select Case strCodigo
            Case "01" : Return "La CUIT informada no corresponde a un responsable inscripto en el iva activo."
            Case "02" : Return "La CUIT informada no se encuentra autorizada a emitir comprobantes electronicos originales o el periodo de inicio autorizado es posterior al de la generacion de la solicitud."
            Case "03" : Return "La CUIT informada registra inconvenientes con el domicilio fiscal."
            Case "04" : Return "El punto de venta informado no se encuentra declarado para ser utilizado en el presente regimen."
            Case "05" : Return "La fecha del comprobante indicada no puede ser anterior en mas de cinco dias, si se trata de una venta, o anterior o posterior en mas de diez dias, si se trata de una prestacion de servicios, consecutivos de la fecha de remision del archivo    Art. 22 de la RG N� 2177-."
            Case "06" : Return "La CUIT informada no se encuentra autorizada a emitir comprobantes clase 'A'."
            Case "07" : Return "Para la clase de comprobante solicitado -Comprobante clase A- debera consignar en el campo codigo de documento identificatorio del comprador el codigo '80'."
            Case "08" : Return "La CUIT indicada en el campo N� de identificacion del comprador es invalida."
            Case "09" : Return "La CUIT indicada en el campo N� de identificacion del comprador no existe en el padron unico de contribuyentes."
            Case "10" : Return "La CUIT indicada en el campo N� de identificacion del comprador no corresponde a un responsable inscripto en el iva activo."
            Case "11" : Return "El N� de comprobante � la fecha de emisi�n informados no son correlativos al ultimo N� de comprobante registrado solicitado para ese tipo de comprobante y punto de venta."
            Case "12" : Return "El rango informado se encuentra autorizado con anterioridad para la misma CUIT, tipo de comprobante y punto de venta."
            Case "13" : Return "" 'La CUIT INDICADA SE ENCUENTRA COMPRENDIDA EN EL REGIMEN ESTABLECIDO POR LA RESOLUCION GENERAL N� 2177 Y/O EN EL TITULO I DE LA RESOLUCION GENERAL N� 1361 ART. 24 DE LA RG N� 2177-."
            Case Else
                Return "CODIGO AFIP " & strCodigo
        End Select
    End Function

    Public Function NombreCortoComprobante(ByVal CodigoComprobante As Integer) As String
        Select Case CodigoComprobante
            Case 1
                Return "FV-A"
            Case 2
                Return "ND-A"
            Case 3
                Return "NC-A"
            Case 4
                Return "RC-A"
            Case 5
                Return "NV-A"
            Case 6
                Return "FV-B"
            Case 7
                Return "ND-B"
            Case 8
                Return "NC-B"
            Case 9
                Return "RC-B"
            Case 10
                Return "NV-B"
            Case 39
                Return "OC-A"
            Case 40
                Return "OC-B"
            Case 60
                Return "LP-A"
            Case 61
                Return "LP-B"
            Case 63
                Return "LI-A"
            Case 64
                Return "LI-B"
            Case Else
                Return "NN-N"
        End Select
    End Function

End Module
