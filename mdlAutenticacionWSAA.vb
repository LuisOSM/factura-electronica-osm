Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Xml
Imports System.Net
Imports System.Security
Imports System.Security.Cryptography
Imports System.Security.Cryptography.Pkcs
Imports System.Security.Cryptography.X509Certificates
Imports System.IO
Imports System.Runtime.InteropServices

Public Class MyPolicy
    Implements ICertificatePolicy
    Public Function CheckValidationResult(ByVal srvPoint As ServicePoint,
      ByVal cert As X509Certificate, ByVal request As WebRequest,
      ByVal certificateProblem As Integer) _
      As Boolean Implements ICertificatePolicy.CheckValidationResult
        'Return True to force the certificate to be accepted.
        Return True
    End Function
End Class

Class LoginTicket

    Public UniqueId       As UInt32 ' Entero de 32 bits sin signo que identifica el requerimiento
    Public GenerationTime As DateTime ' Momento en que fue generado el requerimiento
    Public ExpirationTime As DateTime ' Momento en el que exoira la solicitud
    Public Service As String ' Identificacion del WSN para el cual se solicita el TA
    Public Sign As String ' Firma de seguridad recibida en la respuesta
    Public Token As String ' Token de seguridad recibido en la respuesta

    Public XmlLoginTicketRequest As XmlDocument = Nothing
    Public XmlLoginTicketResponse As XmlDocument = Nothing
    Public RutaDelCertificadoFirmante As String
    Public XmlStrLoginTicketRequestTemplate As String = "<loginTicketRequest><header><uniqueId></uniqueId><generationTime></generationTime><expirationTime></expirationTime></header><service></service></loginTicketRequest>"

    Private _verboseMode As Boolean = True

    Private Shared _globalUniqueID As UInt32 = 0 ' OJO! NO ES THREAD-SAFE

    Public Function ObtenerLoginTicketResponse( _
        ByVal argServicio As String, _
        ByVal argUrlWsaa As String, _
        ByVal argRutaCertX509Firmante As String, _
        ByVal argPassword As SecureString, _
        ByVal argProxy As String, _
        ByVal argProxyUser As String, _
        ByVal argProxyPassword As String, _
        ByVal argVerbose As Boolean, _
        ByVal OffsetHoraAfip As Long) As String

        Me.RutaDelCertificadoFirmante = argRutaCertX509Firmante
        Me._verboseMode = argVerbose
        CertificadosX509Lib.VerboseMode = argVerbose

        Dim cmsFirmadoBase64 As String
        Dim loginTicketResponse As String
        Dim xmlNodoUniqueId As XmlNode
        Dim xmlNodoGenerationTime As XmlNode
        Dim xmlNodoExpirationTime As XmlNode
        Dim xmlNodoService As XmlNode

        ' PASO 1: Genero el Login Ticket Request
        Try
            _globalUniqueID += 1

            XmlLoginTicketRequest = New XmlDocument()
            XmlLoginTicketRequest.LoadXml(XmlStrLoginTicketRequestTemplate)

            xmlNodoUniqueId = XmlLoginTicketRequest.SelectSingleNode("//uniqueId")
            xmlNodoGenerationTime = XmlLoginTicketRequest.SelectSingleNode("//generationTime")
            xmlNodoExpirationTime = XmlLoginTicketRequest.SelectSingleNode("//expirationTime")
            xmlNodoService = XmlLoginTicketRequest.SelectSingleNode("//service")

            xmlNodoGenerationTime.InnerText = DateTime.Now.AddMinutes(-10).AddMilliseconds(OffsetHoraAfip).ToString("s")
            xmlNodoExpirationTime.InnerText = DateTime.Now.AddMinutes(+10).AddMilliseconds(OffsetHoraAfip).ToString("s")
            xmlNodoUniqueId.InnerText = CStr(_globalUniqueID)
            xmlNodoService.InnerText = argServicio
            Me.Service = argServicio

            If Me._verboseMode Then
                LogToFile(Now & "---- LoginTicketrequest: " & vbCrLf & XmlLoginTicketRequest.OuterXml & vbCrLf & "---- Fin LoginTicketRequest")
            End If

        Catch excepcionAlGenerarLoginTicketRequest As Exception
            Throw New Exception("***Error GENERANDO el LoginTicketRequest : " + excepcionAlGenerarLoginTicketRequest.Message + excepcionAlGenerarLoginTicketRequest.StackTrace)
        End Try

        ' PASO 2: Firmo el Login Ticket Request
        Try
            If Me._verboseMode Then
                LogToFile("***Leyendo certificado: " & RutaDelCertificadoFirmante)
            End If

            Dim certFirmante As X509Certificate2 = CertificadosX509Lib.ObtieneCertificadoDesdeArchivo(RutaDelCertificadoFirmante, argPassword)

            If Me._verboseMode Then
                LogToFile("***Firmando: ")
                LogToFile(XmlLoginTicketRequest.OuterXml)
            End If

            ' Convierto el login ticket request a bytes, para firmar
            Dim EncodedMsg As Encoding = Encoding.UTF8
            Dim msgBytes As Byte() = EncodedMsg.GetBytes(XmlLoginTicketRequest.OuterXml)

            ' Firmo el msg y paso a Base64
            Dim encodedSignedCms As Byte() = CertificadosX509Lib.FirmaBytesMensaje(msgBytes, certFirmante)
            cmsFirmadoBase64 = Convert.ToBase64String(encodedSignedCms)

        Catch excepcionAlFirmar As Exception
            Throw New Exception("***Error FIRMANDO el LoginTicketRequest : " + excepcionAlFirmar.Message)
        End Try

        ' PASO 3: Invoco al WSAA para obtener el Login Ticket Response
        Try
            If Me._verboseMode Then
                LogToFile("***Llamando al WSAA en URL: " & argUrlWsaa)
                LogToFile("***Argumento en el request:")
                LogToFile(cmsFirmadoBase64)
            End If

            Dim servicioWsaa As New wsaa.LoginCMSService()
            servicioWsaa.Url = argUrlWsaa
            If argProxy IsNot Nothing Then
                servicioWsaa.Proxy = New WebProxy(argProxy, True)
                If argProxyUser IsNot Nothing Then
                    Dim Credentials As New NetworkCredential(argProxyUser, argProxyPassword)
                    servicioWsaa.Proxy.Credentials = Credentials
                End If
            End If

            'Con esta cosa horrible, evito que se cancele todo por problemas en el certificado de la afip (x ej que este vencido).
            System.Net.ServicePointManager.CertificatePolicy = New MyPolicy

            loginTicketResponse = servicioWsaa.loginCms(cmsFirmadoBase64)

            If Me._verboseMode Then
                LogToFile("***LoguinTicketResponse: ")
                LogToFile(loginTicketResponse)
            End If

        Catch excepcionAlInvocarWsaa As Exception
            Throw New Exception("***Error INVOCANDO al servicio WSAA : " + excepcionAlInvocarWsaa.Message)
        End Try


        ' PASO 4: Analizo el Login Ticket Response recibido del WSAA
        Try
            XmlLoginTicketResponse = New XmlDocument()
            XmlLoginTicketResponse.LoadXml(loginTicketResponse)
            Me.UniqueId = UInt32.Parse(XmlLoginTicketResponse.SelectSingleNode("//uniqueId").InnerText)
            Me.GenerationTime = DateTime.Parse(XmlLoginTicketResponse.SelectSingleNode("//generationTime").InnerText)
            Me.ExpirationTime = DateTime.Parse(XmlLoginTicketResponse.SelectSingleNode("//expirationTime").InnerText)
            Me.Sign = XmlLoginTicketResponse.SelectSingleNode("//sign").InnerText
            Me.Token = XmlLoginTicketResponse.SelectSingleNode("//token").InnerText
            XmlLoginTicketResponse.Save(My.Application.Info.DirectoryPath & "\WSAA_TA.xml")
        Catch excepcionAlAnalizarLoginTicketResponse As Exception
            Throw New Exception("***Error ANALIZANDO el LoginTicketResponse : " + excepcionAlAnalizarLoginTicketResponse.Message)
        End Try

        Return loginTicketResponse

    End Function

    Public Function AutorizacionValidaEncontrada() As Boolean
        Try
            Dim strFilename As String = My.Application.Info.DirectoryPath & "\WSAA_TA.xml"
            If File.Exists(strFilename) = False Then
                Return False
            End If
            XmlLoginTicketResponse = New XmlDocument()
            XmlLoginTicketResponse.Load(strFilename)
            Me.UniqueId = UInt32.Parse(XmlLoginTicketResponse.SelectSingleNode("//uniqueId").InnerText)
            Me.GenerationTime = DateTime.Parse(XmlLoginTicketResponse.SelectSingleNode("//generationTime").InnerText)
            Me.ExpirationTime = DateTime.Parse(XmlLoginTicketResponse.SelectSingleNode("//expirationTime").InnerText)
            Me.Sign = XmlLoginTicketResponse.SelectSingleNode("//sign").InnerText
            Me.Token = XmlLoginTicketResponse.SelectSingleNode("//token").InnerText
            If Me.ExpirationTime.ToUniversalTime() > Date.UtcNow Then
                LogToFile("WSAA_TA valido   encontrado, expira   en " & Me.ExpirationTime.ToString() & "(UTC: " & Me.ExpirationTime.ToUniversalTime() & ") siendo ahora " & Now & " (UTC: " & Date.UtcNow & ")")
                Return True
            Else
                LogToFile("WSAA_TA invalido encontrado, expirado en " & Me.ExpirationTime.ToString() & "(UTC: " & Me.ExpirationTime.ToUniversalTime() & ") siendo ahora " & Now & " (UTC: " & Date.UtcNow & ")")
            End If
        Catch ex As Exception
            LogToFile("***Error ANALIZANDO WSAA_TA: " & ex.Message)
        End Try
    End Function

End Class

Class CertificadosX509Lib

    Public Shared VerboseMode As Boolean = False

    Public Shared Function FirmaBytesMensaje( _
    ByVal argBytesMsg As Byte(), _
    ByVal argCertFirmante As X509Certificate2 _
    ) As Byte()
        Try
            ' Pongo el mensaje en un objeto ContentInfo (requerido para construir el obj SignedCms)
            Dim infoContenido As New ContentInfo(argBytesMsg)
            Dim cmsFirmado As New SignedCms(infoContenido)

            ' Creo objeto CmsSigner que tiene las caracteristicas del firmante
            Dim cmsFirmante As New CmsSigner(argCertFirmante)
            cmsFirmante.IncludeOption = X509IncludeOption.EndCertOnly

            If VerboseMode Then
                LogToFile("***Firmando bytes del mensaje...")
            End If
            ' Firmo el mensaje PKCS #7
            cmsFirmado.ComputeSignature(cmsFirmante)

            If VerboseMode Then
                LogToFile("***OK mensaje firmado")
            End If

            ' Encodeo el mensaje PKCS #7.
            Return cmsFirmado.Encode()
        Catch excepcionAlFirmar As Exception
            Throw New Exception("***Error al firmar: " & excepcionAlFirmar.Message)
            Return Nothing
        End Try
    End Function

    Public Shared Function ObtieneCertificadoDesdeArchivo( _
    ByVal argArchivo As String, _
    ByVal argPassword As SecureString _
    ) As X509Certificate2
        Dim objCert As New X509Certificate2
        Try
            If argPassword.IsReadOnly Then
                objCert.Import(My.Computer.FileSystem.ReadAllBytes(argArchivo), argPassword, X509KeyStorageFlags.PersistKeySet)
            Else
                objCert.Import(My.Computer.FileSystem.ReadAllBytes(argArchivo))
            End If
            Return objCert
        Catch excepcionAlImportarCertificado As Exception
            Throw New Exception(excepcionAlImportarCertificado.Message & " " & excepcionAlImportarCertificado.StackTrace)
            Return Nothing
        End Try
    End Function

End Class

Public Module AutenticacionWSAA

    ' Valores por defecto, globales en esta clase
    Const DEFAULT_URLWSAAWSDL As String = "https://wsaa.afip.gov.ar/ws/services/LoginCms?wsdl"
    'Const DEFAULT_URLWSAAWSDL As String = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms"
    Const DEFAULT_SERVICIO As String = "wsfe"
    Const DEFAULT_CERTSIGNER As String = "Certificados\osam.pfx"
    Const DEFAULT_PROXY As String = Nothing
    Const DEFAULT_PROXY_USER As String = Nothing
    Const DEFAULT_PROXY_PASSWORD As String = ""
    Const DEFAULT_VERBOSE As Boolean = True

    Public Function ObtenerAutorizacion(ByRef ErrorDescription As String) As Boolean

        Dim strUrlWsaaWsdl As String = DEFAULT_URLWSAAWSDL
        Dim strIdServicioNegocio As String = DEFAULT_SERVICIO
        Dim strRutaCertSigner As String = DEFAULT_CERTSIGNER
        Dim strPasswordSecureString As New SecureString
        Dim strProxy As String = DEFAULT_PROXY
        Dim strProxyUser As String = DEFAULT_PROXY_USER
        Dim strProxyPassword As String = DEFAULT_PROXY_PASSWORD
        Dim blnVerboseMode As Boolean = DEFAULT_VERBOSE

        Dim objTicketRespuesta As LoginTicket
        Dim strTicketRespuesta As String

        Try

            If blnVerboseMode Then
                LogToFile("***Servicio a acceder: " & strIdServicioNegocio)
                LogToFile("***URL del WSAA: " & strUrlWsaaWsdl)
                LogToFile("***Ruta del certificado: " & My.Application.Info.DirectoryPath & "\" & strRutaCertSigner)
                LogToFile("***Modo verbose: " & blnVerboseMode)
            End If

            objTicketRespuesta = New LoginTicket

            If blnVerboseMode Then
                LogToFile("***Accediendo a " & strUrlWsaaWsdl)
            End If

            strTicketRespuesta = objTicketRespuesta.ObtenerLoginTicketResponse( _
                strIdServicioNegocio, _
                strUrlWsaaWsdl, _
                My.Application.Info.DirectoryPath & "\" & strRutaCertSigner, _
                strPasswordSecureString, _
                strProxy, _
                strProxyUser, _
                strProxyPassword, _
                blnVerboseMode, _
                OffsetHoraAfip)

            If blnVerboseMode Then
                LogToFile("***CONTENIDO DEL TICKET RESPUESTA:")
                LogToFile("   Token: " & objTicketRespuesta.Token)
                LogToFile("   Sign: " & objTicketRespuesta.Sign)
                LogToFile("   GenerationTime: " & CStr(objTicketRespuesta.GenerationTime))
                LogToFile("   ExpirationTime: " & CStr(objTicketRespuesta.ExpirationTime))
                LogToFile("   Service: " & objTicketRespuesta.Service)
                LogToFile("   UniqueID: " & CStr(objTicketRespuesta.UniqueId))
            End If
            'Me.UniqueId = objTicketRespuesta.UniqueId
            'Me.GenerationTime = objTicketRespuesta.GenerationTime
            'Me.ExpirationTime = DateTime.Parse(objTicketRespuesta.SelectSingleNode("//expirationTime").InnerText)
            'Me.Sign = objTicketRespuesta.SelectSingleNode("//sign").InnerText
            'Me.Token = objTicketRespuesta.SelectSingleNode("//token").InnerText

            Return True

        Catch excepcionAlObtenerTicket As Exception

            LogToFile("***EXCEPCION AL OBTENER TICKET:")
            LogToFile(excepcionAlObtenerTicket.Message)
            ErrorDescription = "Error al Obtener Ticket de Acceso AFIP." & vbCrLf & _
                               "Detalle del Error: " & excepcionAlObtenerTicket.Message
            Return False

        End Try
    End Function

End Module