Imports System.Diagnostics
Imports System.Text
Imports System.IO
Imports IEFacturaElectronicaTray.LoginTicket
Imports IEFacturaElectronicaTray.SendMail
Imports IEFacturaElectronicaTray.InternetTime

Module mdlIEFEMain
    Private            mobNotifyIcon   As NotifyIcon
    Private WithEvents mobContextMenu  As ContextMenu
    Public WithEvents  mobTimer        As Timers.Timer
    Public WithEvents  fswTimer        As Timers.Timer
    Public WithEvents  fswInFiles      As FileSystemWatcher
    Public WithEvents  fswOutFiles     As FileSystemWatcher
    Public             ProcesandoLista As Boolean
    Private            Estado          As New frmEstado
    Public             OffsetHoraAfip  As Long
    Public             NextTimeout     As Date
    Public             NextCheckTime   As Date

    Public Structure PuntoDeVenta_t
        Public FVA_01 As Integer
        Public NDA_02 As Integer
        Public NCA_03 As Integer
        Public RCA_04 As Integer
        Public NVA_05 As Integer
        Public FVB_06 As Integer
        Public NDB_07 As Integer
        Public NCB_08 As Integer
        Public RCB_09 As Integer
        Public NVB_10 As Integer
        Public OCA_39 As Integer
        Public OCB_40 As Integer
        Public LPA_60 As Integer
        Public LPB_61 As Integer
        Public LIA_63 As Integer
        Public LIB_64 As Integer
    End Structure

    Public Structure AppConfig_t
        Public InPath             As String
        Public OutPath            As String
        Public ErrorPath          As String
        Public TempPath           As String
        Public HistoryPath        As String
        Public CUIT               As String
        Public OffsetNumeracion   As Integer
        Public PuntosDeVenta()    As PuntoDeVenta_t
        Public Host               As String
        Public Puerto             As Integer
        Public User               As String
        Public Password           As String
        Public NotifyTo           As String
        Public RetryInterval      As Integer
        Public CheckInterval      As Integer
        Public RenumerarSegunAfip As Integer
        Public DoNotUpdateUI      As Integer
    End Structure

    Public AppConfig As AppConfig_t

    Public Structure InData_t
        Public TipoDocumento As Integer      ' Codigo de documento identificatiorio del comprador (Segun Tabla Solapa "Documentos")
        Public NumeroDocumento As String     ' N� de identificacion del comprador
        Public TipoComprobante As Integer    ' Tipo de comprobante (Segun Tabla Solapa "Comprobantes")
        Public PuntoDeVenta As Integer       ' Punto de venta
        Public NumeroComDesde As Integer     ' Numero de comprobante desde
        Public NumeroComHasta As Integer     ' Numero de comprobante hasta
        Public ImporteTotal As Decimal       ' Importe total del comprobante
        Public TotalNoGravado As Decimal     ' Importe total de conceptos que no integran el precio neto gravado
        Public TotalNeto As Decimal          ' Importe total neto gravado
        Public TotalIVA As Decimal           ' Importe liquidado IVA
        Public TotalRNIoPercNoCat As Decimal ' Impuesto liquidado a RNI o percepcion a no categorizados
        Public TotalExento As Decimal        ' Importe de operaciones exentas
        Public FechaComprobante As Date      ' Fecha del comprobante
        Public FechaVencimiento As Date      ' Fecha de vencimiento 
        Public FileName As String
        Public TotalNeto03 As Decimal          ' Importe total neto gravado  0.00%
        Public TotalIVA03 As Decimal           ' Importe liquidado IVA       0.00%
        Public TotalNeto04 As Decimal          ' Importe total neto gravado 10.50%
        Public TotalIVA04 As Decimal           ' Importe liquidado IVA      10.50%
        Public TotalNeto05 As Decimal          ' Importe total neto gravado 21.00%
        Public TotalIVA05 As Decimal           ' Importe liquidado IVA      21.00%
        Public TotalNeto06 As Decimal          ' Importe total neto gravado 27.00%
        Public TotalIVA06 As Decimal           ' Importe liquidado IVA      27.00%
        Public BaseImponibleTributo01 As Decimal 'Base imponible Impuestos Nacionales
        Public AlicuotaTributo01 As Decimal      'Alicuota Impuestos Nacionales
        Public TotalTributo01 As Decimal         'Importe Impuestos Nacionales
        Public BaseImponibleTributo02 As Decimal 'Base imponible Impuestos Provinciales
        Public AlicuotaTributo02 As Decimal      'Alicuota Impuestos Provinciales
        Public TotalTributo02 As Decimal         'Importe Impuestos Provinciales
        Public BaseImponibleTributo03 As Decimal 'Base imponible Impuestos Municipales
        Public AlicuotaTributo03 As Decimal      'Alicuota Impuestos Municipales
        Public TotalTributo03 As Decimal         'Importe Impuestos Municipales
        Public BaseImponibleTributo04 As Decimal 'Base imponible Impuestos Internos
        Public AlicuotaTributo04 As Decimal      'Alicuota Impuestos Internos
        Public TotalTributo04 As Decimal         'Importe Impuestos Internos
        Public BaseImponibleTributo99 As Decimal 'Base imponible Impuestos Varios
        Public AlicuotaTributo99 As Decimal      'Alicuota Impuestos Varios
        Public TotalTributo99 As Decimal         'Importe Impuestos Varios
        Public CallerId As String                'Paso el dato al out data para que pueda decidir quien imprime el comprobante
        Public CBU As String                     'CBU a informar
        Public CBUAlias As String                'Alias de CBU a informar
        Public FechaComprobanteAsoc As Date      'Fecha del comprobante Asociado
        Public TipoComprobanteAsoc As Integer    'Tipo de comprobante (Segun Tabla Solapa "Comprobantes")
        Public PuntoDeVentaAsoc As Integer       'Punto de venta
        Public NumeroCompAsoc As Integer         'Numero de comprobante desde
        Public CodigoAnulacion As String         'Codigo Anulacion FCE
    End Structure

    Public Structure OutData_t
        Public CAE As String
        Public TipoComprobante As Integer
        Public PuntoDeVenta As Integer
        Public NumeroComDesde As Integer
        Public NumeroComHasta As Integer
        Public Vencimiento As String
        Public ErrorCode As String
        Public ErrorDesc As String
        Public CallerId As String
    End Structure

    Public InData As InData_t
    Public OutData As OutData_t

    Public Sub Main()
        Try
            If Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName).Length > 1 Then
                LogToFile(Now & " Intento de iniciar varias instancias detenido.")
                End
            End If
            LogToFile(Now & " Iniciando IEFE.")
            mobNotifyIcon = New NotifyIcon()
            mobNotifyIcon.Visible = False
            mobContextMenu = New ContextMenu()
            CreateMenu()
            mobNotifyIcon.ContextMenu = mobContextMenu
            mobNotifyIcon.Icon = frmEstado.Icon 'New Icon("ie1.ico")

            mobNotifyIcon.Visible = True

            InitConfiguracion()

            SetUpTimers()

            SincronizarRelojes()

            'init in data
            InData.FileName = vbNullString

            fswInFiles = New FileSystemWatcher()
            InitFswInFiles()
            fswOutFiles = New FileSystemWatcher()
            InitFswOutFiles()

            rutShowStatus()
            Application.DoEvents()
            'ProcesarDirectorio() ' Procesar los que ya esten en el directorio de antes
            Application.Run()
        Catch obEx As Exception
            LogToFile("Main.Error: " & obEx.Message.ToString)
            Process.Start(My.Application.Info.DirectoryPath & "\CrashMonitor.exe")
            End
        End Try
    End Sub

    Private Sub InitConfiguracion()
        Dim srdTxt As StreamReader
        Dim strTxt As String

        'Valores x defecto
        With AppConfig
            .InPath = "_CaeRequests" 'My.Application.Info.DirectoryPath
            .OutPath = "_CaeResults"
            .HistoryPath = "_CaeRequests\Historico"
            .ErrorPath = "_Errores"
            .TempPath = "_Temp"
            .CUIT = "30711418195" 'CUIT DE OSAM
            '.CUIT = "20352438120" 'CUIT DE OSAM
            .OffsetNumeracion = 0
            ReDim .PuntosDeVenta(10)
            .Host = "mail.inforemp.com.ar"
            .Puerto = 25
            .User = "pauldahuach@inforemp.com.ar"
            .Password = "inforemp2009"
            .NotifyTo = "sistemas@inforemp.com.ar"
            .RetryInterval = 15
            .CheckInterval = 2
            .RenumerarSegunAfip = 0
            .DoNotUpdateUI = 0
        End With

        Try
            srdTxt = File.OpenText(My.Application.Info.DirectoryPath & "\AppConfig.txt")
            Try
                Do While srdTxt.Peek <> -1
                    strTxt = srdTxt.ReadLine()
                    If strTxt.Contains("=") Then
                        Dim strPar() As String = strTxt.Split("=")
                        Select Case strPar(0)
                            Case "In Path"
                                AppConfig.InPath = strPar(1)
                            Case "Out Path"
                                AppConfig.OutPath = strPar(1)
                            Case "History Path"
                                AppConfig.HistoryPath = strPar(1)
                            Case "Error Path"
                                AppConfig.ErrorPath = strPar(1)
                            Case "Temp Path"
                                AppConfig.TempPath = strPar(1)
                            Case "CUIT"
                                AppConfig.CUIT = strPar(1)
                            Case "Offset Numeracion"
                                AppConfig.OffsetNumeracion = strPar(1)
                            Case "Mail Host"
                                AppConfig.Host = strPar(1)
                            Case "Mail Puerto"
                                AppConfig.Puerto = strPar(1)
                            Case "Mail User"
                                AppConfig.User = strPar(1)
                            Case "Mail Password"
                                AppConfig.Password = strPar(1)
                            Case "En error notificar a"
                                AppConfig.NotifyTo = strPar(1)
                            Case "RetryInterval"
                                AppConfig.RetryInterval = strPar(1)
                            Case "CheckInterval"
                                AppConfig.CheckInterval = strPar(1)
                            Case "Renumerar segun AFIP"
                                AppConfig.RenumerarSegunAfip = strPar(1)
                            Case "Do Not Update UI"
                                AppConfig.DoNotUpdateUI = strPar(1)
                            Case Else
                                Dim j As Integer = 0
                                For j = 0 To 9
                                    Select Case strPar(0)
                                        Case "PDV(" & Format$(j, "00") & ").FVA_01"
                                            AppConfig.PuntosDeVenta(j).FVA_01 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").NDA_02"
                                            AppConfig.PuntosDeVenta(j).NDA_02 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").NCA_03"
                                            AppConfig.PuntosDeVenta(j).NCA_03 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").RCA_04"
                                            AppConfig.PuntosDeVenta(j).RCA_04 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").NVA_05"
                                            AppConfig.PuntosDeVenta(j).NVA_05 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").FVB_06"
                                            AppConfig.PuntosDeVenta(j).FVB_06 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").NDB_07"
                                            AppConfig.PuntosDeVenta(j).NDB_07 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").NCB_08"
                                            AppConfig.PuntosDeVenta(j).NCB_08 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").RCB_09"
                                            AppConfig.PuntosDeVenta(j).RCB_09 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").NVB_10"
                                            AppConfig.PuntosDeVenta(j).NVB_10 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").OCA_39"
                                            AppConfig.PuntosDeVenta(j).OCA_39 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").OCB_40"
                                            AppConfig.PuntosDeVenta(j).OCB_40 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").LPA_60"
                                            AppConfig.PuntosDeVenta(j).LPA_60 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").LPB_61"
                                            AppConfig.PuntosDeVenta(j).LPB_61 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").LIA_63"
                                            AppConfig.PuntosDeVenta(j).LIA_63 = strPar(1)
                                        Case "PDV(" & Format$(j, "00") & ").LIB_64"
                                            AppConfig.PuntosDeVenta(j).LIB_64 = strPar(1)
                                    End Select
                                Next
                        End Select
                    End If
                Loop
            Catch ex As Exception
                LogToFileAndScreen("InitConfiguracion.Error: " & ex.Message, "")
            End Try
            srdTxt.Close()

        Catch ex As Exception
            LogToFileAndScreen(ex.Message, "")
        End Try
    End Sub

    Public Sub CambiarConfiguracion(ByVal InPath As String, ByVal OutPath As String, ByVal HistoryPath As String, ByVal CUIT As String)
        With AppConfig
            If .InPath <> InPath Then
                Try
                    fswInFiles.Path = InPath
                Catch ex As Exception
                    LogToFile("CambiarConfiguracion.Error:" & ex.Message)
                End Try
                .InPath = InPath
            End If
            If .OutPath <> OutPath Then
                Try
                    fswOutFiles.Path = OutPath
                Catch ex As Exception
                    LogToFile("CambiarConfiguracion.Error:" & ex.Message)
                End Try
                .OutPath = OutPath
            End If
            .HistoryPath = HistoryPath
            .CUIT = CUIT
        End With
        'Guardar en disco...
    End Sub

    Private Sub SetUpTimers()
        Try
            mobTimer = New Timers.Timer()
            With mobTimer
                .AutoReset = False
                .Interval = 1000 * AppConfig.RetryInterval
                '.Start()
            End With

            fswTimer = New Timers.Timer()
            With fswTimer
                .AutoReset = True
                .Interval = 1000 * AppConfig.CheckInterval
                .Start()
            End With
        Catch obEx As Exception
            LogToFile("Main.SetupTimer.Error: " & obEx.Message)
        End Try
    End Sub

    Private Sub CreateMenu()
        Try
            mobContextMenu.MenuItems.Add(New MenuItem("OSAM"))
            mobContextMenu.MenuItems.Add(New MenuItem("Configuraci�n", New EventHandler(AddressOf rutShowConfiguracion)))
            mobContextMenu.MenuItems.Add(New MenuItem("Estado", New EventHandler(AddressOf ShowStatus)))
            'mobContextMenu.MenuItems.Add(New MenuItem("Abrir Archivo de Facturaci�n Global (Electronico.txt)", New EventHandler(AddressOf rutAbrirArchivoGlobal)))
            'mobContextMenu.MenuItems.Add(New MenuItem("Pedir CAE en un solo paquete", New EventHandler(AddressOf rutPedirCAEenLote)))
            'mobContextMenu.MenuItems.Add(New MenuItem("Exportar CAE Facturaci�n Global (F136_XXXXXXXXX.txt)", New EventHandler(AddressOf rutExportarRespuestaGlobal)))
            mobContextMenu.MenuItems.Add(New MenuItem("Actualizar", New EventHandler(AddressOf rutReintentar)))
            'mobContextMenu.MenuItems.Add(New MenuItem("Carga Manual", New EventHandler(AddressOf rutShowCargaManual)))
            mobContextMenu.MenuItems.Add(New MenuItem("Salir", New EventHandler(AddressOf rutSalir)))
        Catch obEx As Exception
            Throw obEx
        End Try
    End Sub

    Private Sub UpdateInterface()
        Try
            Select Case 0
                Case 0
                    'mobContextMenu.MenuItems(1).Enabled = False
            End Select
        Catch obEx As Exception
            Throw obEx
        End Try
    End Sub

    Private Sub rutShowConfiguracion(ByVal sender As Object, ByVal e As EventArgs)
        Try
            frmOpciones.Show()
        Catch obEx As Exception
            Throw obEx
        End Try
    End Sub

    Private Sub rutShowCargaManual(ByVal sender As Object, ByVal e As EventArgs)
        Try
            frmRequestManual.Show()
        Catch obEx As Exception
            Throw obEx
        End Try
    End Sub

    Private Sub ShowStatus(ByVal sender As Object, ByVal e As EventArgs)
        rutShowStatus()
    End Sub

    Private Sub rutShowStatus()
        Try
            Debug.Print("Show Status")
            If Estado.IsDisposed Then Estado = New frmEstado()
            Estado.RefreshBandejaDeEntrada()
            Estado.RefreshBandejaDeSalida()
            Estado.Show()
            Estado.Refresh()
            Application.DoEvents()
        Catch obEx As Exception
            Throw obEx
        End Try
    End Sub

    Private Sub rutReintentar(ByVal sender As Object, ByVal e As EventArgs)
        mobContextMenu.MenuItems(2).Enabled = False
        Try
            LogToFile(Now & ".ini Menu.Actualizar")
            ProcesarDirectorio()
            LogToFile(Now & ".fin Menu.Actualizar")
        Catch obEx As Exception
            LogToFile(Now & ". Error en Menu.Actualizar: " & obEx.Message)
        End Try
        mobContextMenu.MenuItems(2).Enabled = True
    End Sub

    Private Sub rutCreateTestFile(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim Texto As String
            Dim ff As System.IO.StreamWriter = Microsoft.VisualBasic.FileIO.FileSystem.OpenTextFileWriter(My.Application.Info.DirectoryPath & "\TestFile.txt", True)
            Texto = ""
            'With InData
            Texto = Texto & Format$(80, New String("0", 2)) '.TipoDocumento
            Texto = Texto & Format$(30687310434, New String("0", 11)) '.NumeroDocumento
            Texto = Texto & Format$(1, New String("0", 2)) '.TipoComprobante
            Texto = Texto & Format$(6, New String("0", 4)) '.PuntoDeVenta
            Texto = Texto & Format$(63483, New String("0", 8)) '.NumeroComprobante
            Texto = Texto & Format$(Int(121 * 100), New String("0", 15)) '.ImporteTotal
            Texto = Texto & Format$(Int(0 * 100), New String("0", 15)) '.TotalNoGravado
            Texto = Texto & Format$(Int(100 * 100), New String("0", 15)) '.TotalNeto
            Texto = Texto & Format$(Int(21 * 100), New String("0", 15)) '.TotalIVA
            Texto = Texto & Format$(Int(0 * 100), New String("0", 15)) '.TotalRNIoPercNoCat
            Texto = Texto & Format$(Int(0 * 100), New String("0", 15)) '.TotalExento
            Texto = Texto & Format$(Year(#2/14/2011#), "0000") & Format$(Month(#2/14/2011#), "00") & Format$(DateAndTime.Day(#2/14/2011#), "00") '.FechaComprobante
            'End With
            ff.WriteLine(Texto)
            ff.Close()
        Catch obEx As Exception
            Throw obEx
        End Try
    End Sub

    Private Sub rutPedirCAEenLote(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim diiPath As New DirectoryInfo(AppConfig.TempPath)
            Dim fiiList As FileInfo() = diiPath.GetFiles()
            Dim abcList As List(Of String) = New List(Of String)
            For Each fi As FileInfo In fiiList
                abcList.Add(fi.Name)
            Next
            Dim arrList As String() = abcList.ToArray()
            Array.Sort(arrList)
            abcList = New List(Of String)(arrList)

            Dim LT As LoginTicket = New LoginTicket()
            Dim strErrorDescription As String = vbNullString
            'Chequear el ticket de login
            If Not LT.AutorizacionValidaEncontrada() = True Then
                mdlWSFEv1.ResetToken()
                If AutenticacionWSAA.ObtenerAutorizacion(strErrorDescription) = False Then
                    LogToFileAndScreen(strErrorDescription, InData.FileName)
                    Exit Sub
                End If
            End If
            If Not mdlWSFEv1.Inicializado() Then
                mdlWSFEv1.Inicializar(AppConfig.CUIT)
            End If
            If mdlWSFEv1.Inicializado() Then
                If abcList.Count = 0 Then Exit Sub
                Dim Detalle(abcList.Count - 1) As SolicitudCAEDetalle
                Dim intCnt As Integer
                For Each fn As String In abcList
                    If ParseFileV0c(fn) Then
                        'Solicitar CAEs todos juntos
                        With Detalle(intCnt)
                            .FileName = InData.FileName.Replace(".TXT", "")
                            .NumeroDesde = InData.NumeroComDesde
                            .NumeroHasta = InData.NumeroComHasta
                            .EsServicio = False
                            .TipoDoc = InData.TipoDocumento
                            .NroDoc = InData.NumeroDocumento
                            .TipoComprobante = InData.TipoComprobante
                            .PuntoDeVenta = InData.PuntoDeVenta
                            .ImporteTotal = InData.ImporteTotal
                            .ImporteNoGravado = InData.TotalNoGravado
                            .ImporteNeto = InData.TotalNeto
                            .ImporteIVA = InData.TotalIVA
                            .ImporteRNI = InData.TotalRNIoPercNoCat
                            .ImporteEXE = InData.TotalExento
                            .FechaComprobante = InData.FechaComprobante
                            .FechaServDesde = InData.FechaComprobante
                            .FechaServHasta = InData.FechaComprobante
                            .FechaVencimiento = InData.FechaVencimiento
                            .ImporteNeto03 = InData.TotalNeto03
                            .ImporteIVA03 = InData.TotalIVA03
                            .ImporteNeto04 = InData.TotalNeto04
                            .ImporteIVA04 = InData.TotalIVA04
                            .ImporteNeto05 = InData.TotalNeto05
                            .ImporteIVA05 = InData.TotalIVA05
                            .ImporteNeto06 = InData.TotalNeto06
                            .ImporteIVA06 = InData.TotalIVA06
                            .BaseImponibleTributo01 = InData.BaseImponibleTributo01
                            .AlicuotaTributo01 = InData.AlicuotaTributo01
                            .ImporteTributo01 = InData.TotalTributo01
                            .BaseImponibleTributo02 = InData.BaseImponibleTributo02
                            .AlicuotaTributo02 = InData.AlicuotaTributo02
                            .ImporteTributo02 = InData.TotalTributo02
                            .BaseImponibleTributo03 = InData.BaseImponibleTributo03
                            .AlicuotaTributo03 = InData.AlicuotaTributo03
                            .ImporteTributo03 = InData.TotalTributo03
                            .BaseImponibleTributo04 = InData.BaseImponibleTributo04
                            .AlicuotaTributo04 = InData.AlicuotaTributo04
                            .ImporteTributo04 = InData.TotalTributo04
                            .BaseImponibleTributo99 = InData.BaseImponibleTributo99
                            .AlicuotaTributo99 = InData.AlicuotaTributo99
                            .ImporteTributo99 = InData.TotalTributo99
                            .outCAE = ""
                            .outVencimiento = ""
                            .outReferencia = ""
                            .outErrorDescription = ""
                        End With
                    End If
                    intCnt += 1
                    'If PublicarCAE(InData, OutData) Then
                    ' PasarAHistorico(fn)
                    'End If
                    'End If
                Next fn
                Dim outErrorDescription(15) As String
                SolicitarCAEenLote(Detalle, outErrorDescription)
            End If
        Catch ex As Exception
            LogToFileAndScreen(Now & " " & ex.Message, "")
            RestartTimer()
        End Try
    End Sub

    Private Sub rutAbrirArchivoGlobal(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim Texto As String
            Dim FileName As String
            Dim intErrCount As Integer
            Dim ErrDesc As String = ""
            Dim sr As System.IO.StreamReader = Microsoft.VisualBasic.FileIO.FileSystem.OpenTextFileReader(My.Application.Info.DirectoryPath & "\Electronico.txt")
            Do Until sr.EndOfStream
                Texto = sr.ReadLine()
                FileName = Texto.Substring(10 - 1, 2)  'Tipo de Comprobantes
                FileName &= Texto.Substring(13 - 1, 4) 'PDV
                FileName &= Texto.Substring(17 - 1, 8) 'Numero
                FileName &= ".txt"
                If Texto.Substring(10 - 1, 2) = "01" And _
                   Texto.Substring(36 - 1, 2) = "80" _
                   Then

                    If Texto.Substring(38 - 1, 11) = "00000000000" _
                       Then
                        FileName = mdlIEFEMain.AppConfig.ErrorPath & "\" & FileName & ".NumDocZero"
                        intErrCount += 1
                    ElseIf Not IsValidCUIT(Texto.Substring(38 - 1, 11), ErrDesc) Then
                        'Validar que los cuits sean validos al menos para nuestra validacion para evitar: (10015) Factura B (CbteDesde igual a CbteHasta), DocTipo: 80, DocNro 23103298865 no se encuentra registrado en los padrones de AFIP y no corresponde a una cuit pais.
                        FileName = mdlIEFEMain.AppConfig.ErrorPath & "\" & FileName & ".BadCuit"
                        intErrCount += 1
                    Else
                        FileName = mdlIEFEMain.AppConfig.TempPath & "\" & FileName
                    End If
                Else
                    FileName = mdlIEFEMain.AppConfig.TempPath & "\" & FileName
                End If
                'Validar que coincida el total iva con el importe resultante de multiplicar el neto por la alicuota correspondiente para evitar: (10051) Los importes informados en AlicIVA no se corresponden con los porcentajes.
                Dim sw As System.IO.StreamWriter = Microsoft.VisualBasic.FileIO.FileSystem.OpenTextFileWriter(FileName, True, System.Text.Encoding.ASCII)
                sw.WriteLine(Texto)
                sw.Close()
            Loop
            MsgBox("Conversi�n archivo global Exitosa." & vbCrLf & _
                   "Cantidad Registros con Error: " & intErrCount, MsgBoxStyle.Information, "ATENCION")
        Catch obEx As Exception
            Throw obEx
        End Try
    End Sub

    'Private Sub rutExportarRespuestaGlobal(ByVal sender As Object, ByVal e As EventArgs)
    '    ProcesandoLista = True 'setear en false cuando termine
    '    mobTimer.Enabled = False
    '    fswInFiles.EnableRaisingEvents = False
    '    mobContextMenu.MenuItems(2).Enabled = False
    '    Try
    ' 'recupero los archivos alojados en el outpath
    ' Dim diiPath As New DirectoryInfo(AppConfig.OutPath)
    ' Dim fiiList As FileInfo() = diiPath.GetFiles()
    ' Dim abcList As List(Of String) = New List(Of String)
    ' For Each fi As FileInfo In fiiList
    ' abcList.Add(fi.Name)
    ' Next
    ' Dim arrList As String() = abcList.ToArray()
    ' Array.Sort(arrList)
    ' abcList = New List(Of String)(arrList)''''

    'abro el archivo general de destino
    'Dim strFileName As String = AppConfig.OutPath & "\F136_XXXXXXXXX.TXT"
    'Dim ff As System.IO.StreamWriter = _
    ' Microsoft.VisualBasic.FileIO.FileSystem.OpenTextFileWriter(strFileName, False, System.Text.Encoding.ASCII)
    ' For Each fn As String In abcList
    ' 'abro el archivo que contiene el cae
    ' Dim strCAEFileName As String = AppConfig.OutPath & "\" & fn
    ' Dim sfrCAE As System.IO.StreamReader = Microsoft.VisualBasic.FileIO.FileSystem.OpenTextFileReader(strCAEFileName, System.Text.Encoding.ASCII)
    ' Dim strCAE As String = sfrCAE.ReadLine()
    ' sfrCAE.Close()
    ' ff.WriteLine(OutData.CAE & OutData.Vencimiento)
    ' Next fn
    ' ff.Close()
    ' Catch ex As Exception
    ' LogToFileAndScreen(Now & " " & ex.Message, "")
    ' RestartTimer()
    'End Try
    'ProcesandoLista = False
    'fswInFiles.EnableRaisingEvents = True
    'mobContextMenu.MenuItems(2).Enabled = True
    'End Sub


    Private Sub rutSalir(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If mobTimer IsNot Nothing Then
                mobTimer.Stop()
                mobTimer.Dispose()
            End If
            If fswTimer IsNot Nothing Then
                fswTimer.Stop()
                fswTimer.Dispose()
            End If
            mobNotifyIcon.Visible = False
            mobNotifyIcon.Dispose()
            LogToFile(Now & " Finalizando aplicaci�n en forma normal.")
            Application.Exit()
        Catch obEx As Exception
            Throw obEx
        End Try
    End Sub

    Public Sub mobTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles mobTimer.Elapsed
        Try
            If Not ProcesandoLista Then
                ProcesarDirectorio()
            End If
        Catch obEx As Exception
            LogToFile("mobTimer_Elapsed.Error: " & obEx.Message)
        End Try
        'mobTimer.Enabled = False
    End Sub

    Public Sub fswTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles fswTimer.Elapsed
        Try
            'Evito reentrar a procesar el directorio si:
            '* ya se esta procesando la lista
            '* ya est� programado un reintento luego de un error, para no spamear con errores
            If Not ProcesandoLista And Not mobTimer.Enabled Then
                fswTimer.Stop()
                ProcesarDirectorio()
            End If
        Catch obEx As Exception
            LogToFile("fswTimer_Elapsed.Error: " & obEx.Message)
        Finally
            fswTimer.Start()
            NextCheckTime = Now.Add(New TimeSpan(0, 0, fswTimer.Interval / 1000))
        End Try
    End Sub

    ' cambio en carpeta o archivos   
    Private Sub fswInFiles_Changed(ByVal sender As System.Object, _
                                           ByVal e As System.IO.FileSystemEventArgs) _
                                           Handles fswInFiles.Changed
        Debug.Print("Cambio en " & e.Name)
        Estado.RefreshBandejaDeEntrada()
        'ProcesarDirectorio()
    End Sub

    ' creaci�n    
    Private Sub fswInFiles_Created(ByVal sender As Object, _
                                           ByVal e As System.IO.FileSystemEventArgs) _
                                           Handles fswInFiles.Created
        Debug.Print("Creacion de " & e.Name)
        Estado.RefreshBandejaDeEntrada()
    End Sub

    ' eliminaci�n de archivos o carpetas       
    Private Sub fswInFiles_Deleted(ByVal sender As Object, _
                                           ByVal e As System.IO.FileSystemEventArgs) _
                                           Handles fswInFiles.Deleted
        Debug.Print("Eliminacion de " & e.Name)
        Estado.RefreshBandejaDeEntrada()
    End Sub

    ' evento de error
    Private Sub fswInFiles_Error(ByVal sender As Object, _
                                         ByVal e As System.IO.ErrorEventArgs) _
                                         Handles fswInFiles.Error
        LogToFileAndScreen(Now & " Error en Bandeja de Entrada: " & e.GetException.ToString, "")
        ' We need to create new version of the object because the
        ' old one is now corrupted
        fswInFiles = New FileSystemWatcher()
        Do While fswInFiles.EnableRaisingEvents = False
            Try
                ' This will throw an error at the
                ' watcher.NotifyFilter line if it can't get the path.
                InitFswInFiles()
                LogToFileAndScreen(Now & "fswInFiles_Error: En linea nuevamente!", "")
                'ProcesarDirectorio()
            Catch ex As Exception
                LogToFile(Now & "fswInFiles_Error: " & e.GetException.ToString)
                ' Sleep for a bit; otherwise, it takes a bit of
                ' processor time
                System.Threading.Thread.Sleep(60 * 1000 * 5)
            End Try
        Loop
    End Sub

    ' cambio en carpeta o archivos   
    Private Sub fswOutFiles_Changed(ByVal sender As System.Object, _
                                           ByVal e As System.IO.FileSystemEventArgs) _
                                           Handles fswOutFiles.Changed
        'Notificar_Cambio(e.Name, "Cambio")
        Debug.Print("Cambio en " & e.Name)
        Estado.RefreshBandejaDeSalida()
    End Sub

    ' creaci�n    
    Private Sub fswOutFiles_Created(ByVal sender As Object, _
                                           ByVal e As System.IO.FileSystemEventArgs) _
                                           Handles fswOutFiles.Created
        Debug.Print("Creacion de " & e.Name)
        Estado.RefreshBandejaDeSalida()
    End Sub
    ' eliminaci�n de archivos o carpetas       
    Private Sub fswOutFiles_Deleted(ByVal sender As Object, _
                                           ByVal e As System.IO.FileSystemEventArgs) _
                                           Handles fswOutFiles.Deleted
        Debug.Print("Eliminacion de " & e.Name)
        Estado.RefreshBandejaDeSalida()
    End Sub

    ' evento de error
    Private Sub fswOutFiles_Error(ByVal sender As Object, _
                                         ByVal e As System.IO.ErrorEventArgs) _
                                         Handles fswOutFiles.Error
        LogToFileAndScreen(Now & " Error en Bandeja de Salida: " & e.GetException.ToString, "")
        ' We need to create new version of the object because the
        ' old one is now corrupted
        fswOutFiles = New FileSystemWatcher()
        Do While fswOutFiles.EnableRaisingEvents = False
            Try
                ' This will throw an error at the
                ' watcher.NotifyFilter line if it can't get the path.
                InitFswOutFiles()
                LogToFile(Now & "fswOutFiles_Error: En linea nuevamente!")
            Catch ex As Exception
                LogToFile(Now & "fswOutFiles_Error: " & e.GetException.ToString)
                ' Sleep for a bit; otherwise, it takes a bit of
                ' processor time
                System.Threading.Thread.Sleep(60 * 1000 * 5)
            End Try
        Loop
    End Sub

    Private Sub ProcesarDirectorio()
        ProcesandoLista = True 'setear en false cuando termine
        mobTimer.Enabled = False
        fswInFiles.EnableRaisingEvents = False
        mobContextMenu.MenuItems(2).Enabled = False
        Try
            If IsConnectionAvailable() Then
                Dim diiPath As New DirectoryInfo(AppConfig.InPath)
                Dim fiiList As FileInfo() = diiPath.GetFiles()
                Dim abcList As List(Of String) = New List(Of String)
                For Each fi As FileInfo In fiiList
                    abcList.Add(fi.Name)
                Next
                Dim arrList As String() = abcList.ToArray()
                Array.Sort(arrList)
                abcList = New List(Of String)(arrList)
                For Each fn As String In abcList
                    If Not ProcesarArchivo(fn) Then
                        Dim intSufijo As Integer = 0
                        Dim strSufijo As String = ""
                        Do Until File.Exists(AppConfig.ErrorPath & "\" & fn & "_" & OutData.ErrorCode & "_" & strSufijo) = False
                            strSufijo = intSufijo
                            intSufijo += 1
                        Loop
                        If Not Directory.Exists(AppConfig.ErrorPath) Then
                            Directory.CreateDirectory(AppConfig.ErrorPath)
                        End If
                        File.Copy(AppConfig.InPath & "\" & fn, AppConfig.ErrorPath & "\" & fn & "_" & OutData.ErrorCode & "_" & strSufijo)
                        RestartTimer()
                        Exit For
                    End If
                Next fn
            Else
                LogToFile(Now & ". No se encuentra conectado a internet.")
                'post a retry in 2 minutes...
                RestartTimer()
            End If
        Catch ex As Exception
            LogToFileAndScreen(Now & " " & ex.Message, "")
            RestartTimer()
        End Try
        ProcesandoLista = False
        fswInFiles.EnableRaisingEvents = True
        mobContextMenu.MenuItems(2).Enabled = True

        'ProcesandoLista = True 'setear en false cuando termine
        'fswInFiles.EnableRaisingEvents = False
        'mobContextMenu.MenuItems(2).Enabled = False
        'Try
        '    Dim diiPath As New DirectoryInfo(AppConfig.InPath)
        '    Dim fiiList As FileInfo() = diiPath.GetFiles()
        '    For Each fi As FileInfo In fiiList
        '        ProcesarArchivo(fi.Name)
        '    Next fi
        'Catch ex As Exception
        '    LogToFileAndScreen(Now & " " & ex.Message, "")
        'End Try
        'ProcesandoLista = False
        'fswInFiles.EnableRaisingEvents = True
        'mobContextMenu.MenuItems(2).Enabled = True
    End Sub

    Private Function ProcesarArchivo(ByVal strFileName As String) As Boolean
        If ParseFileV0c(strFileName) Then
            If ProcesarEnAFIP(InData, OutData) Then
                If PublicarCAE(InData, OutData) Then
                    If PasarAHistorico(strFileName) Then
                        Application.DoEvents()
                        Return True
                    End If
                End If
            End If
        End If
    End Function

    Private Function ParseFileV0c(ByVal strFileName As String) As Boolean
        '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111112222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222333333333333333333
        '00000000011111111112222222222333333333344444444445555555555666666666677777777778888888888999999999900000000001111111111222222222233333333334444444444555555555566666666667777777777888888888899999999990000000000111111111122222222223333333333444444444455555555556666666666777777777788888888889999999999000000000011111111
        '12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567
        '-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        '120170328012002200001551000015510008023076844259RODRIGUEZ LUIS ALBERTO        00000000001545000000000000000000000000001480000000000000000000000000000000000000000000000000000000000000000000000000065000000000000000000000000000000000000000000000001PES00000000001 000000000000000000000000000000000000000600000000000013000
        '120191007201001500000001000000010008030522428163GALENO ARGENTINA S.A.(OBLIG)  00000003514942900000000000000000000000000000000000000000000000000000000000000000003429212600000000000000000000000085730300000000000000000000000000000000000000000000001PES00000000001 0000000000000020191107000000000000004302500000000342921260140300601658405098972TEMPLO.ASTRO.ROPA   20191121201001500000002S
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       |       |       |   |              |                     |                   |       |  |   |    
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       |       |       |   |              |                     |                   |       |  |   +-- Comprobante Asociado      
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       |       |       |   |              |                     |                   |       |  +--- Punto de Venta Comprobante Asociado
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       |       |       |   |              |                     |                   |       +--- Tipo Comprobante Asociado
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       |       |       |   |              |                     |                   +--- Fecha Comprobante Asociado       
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       |       |       |   |              |                     +--- Alias CBU
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       |       |       |   |              +--- CBU
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       |       |       |   +--- Base Imponible Percepcion IIBB
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       |       |       +--- Alicuota Percepcion
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       |       +--- Usuario
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             |       +--- Fecha de anulaci�n del comprobante
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |||             +--- Fecha de vencimiento o de autorizaci�n
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         ||+--- C�digo de autorizaci�n o de emisi�n
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         |+--- C�digo de operaci�n
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  |         +--- Cantidad de al�cuotas de IVA
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | |  +--- Tipo de cambio
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              | +--- C�digos de moneda
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              |              +--- Tipo de responsable
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              |              +--- Transporte
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              |              +--- Importe de Impuestos Internos
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              |              +--- Importe de percepci�n por Impuestos Municipales
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              |              +--- Importe de percepci�n de Ingresos Brutos
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              |              +--- Imp.de percep. o pagos a cta de impuestos nac.
        '||       |  |   |       |       |  | |          |                             |              |              |              |              |              +--- Importe de operaciones exentas
        '||       |  |   |       |       |  | |          |                             |              |              |              |              +--- Impuesto liq. a RNI o percep. a no categorizados
        '||       |  |   |       |       |  | |          |                             |              |              |              +--- Impuesto liquidado
        '||       |  |   |       |       |  | |          |                             |              |              +--- Importe neto gravado
        '||       |  |   |       |       |  | |          |                             |              +--- Imp.tot. conc que no integran precio neto grav.
        '||       |  |   |       |       |  | |          |                             +--- Importe total de la operaci�n
        '||       |  |   |       |       |  | |          +--- Apell. y nombre o denom. Comprador
        '||       |  |   |       |       |  | +--- N� de identificaci�n del comprador
        '||       |  |   |       |       |  +--- C�d. documento identificatorio del comprador (80:CUIT/86:CUIL/96:DNI)
        '||       |  |   |       |       +--- Cantidad de hojas (no informar)
        '||       |  |   |       +--- N� de comprobante registrado (hasta)
        '||       |  |   +--- N� de comprobante
        '||       |  +--- Punto de venta
        '||       +--- Tipo de comprobante
        '|+--- Fecha del comprobante
        '+--- Tipo de registro

        Dim srdTxt As StreamReader
        Dim strTxt As String
        Dim strTmp As String
        Dim ImportePercIIBB As Decimal = 0
        Try
            InData.FileName = strFileName
            'srdTxt = File.OpenText(AppConfig.TempPath & "\" & strFileName)
            srdTxt = File.OpenText(AppConfig.InPath & "\" & strFileName)
            If srdTxt.Peek <> -1 Then
                strTxt = srdTxt.ReadLine()
                Try
                    With InData
                        strTmp = strTxt.Substring(36 - 1, 2)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Codigo de documento identificatiorio del comprador Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .TipoDocumento = Integer.Parse(strTmp)
                        End If

                        strTmp = strTxt.Substring(38 - 1, 11)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "N� de identificacion del comprador Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .NumeroDocumento = strTmp
                        End If

                        strTmp = strTxt.Substring(10 - 1, 3)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Tipo de comprobante Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .TipoComprobante = Integer.Parse(strTmp)
                        End If

                        strTmp = strTxt.Substring(13 - 1, 4)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Punto de venta Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .PuntoDeVenta = Integer.Parse(strTmp)
                        End If

                        strTmp = strTxt.Substring(17 - 1, 8)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Numero de comprobante Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .NumeroComDesde = Integer.Parse(strTmp)
                        End If

                        strTmp = strTxt.Substring(25 - 1, 8)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Numero de comprobante Hasta Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .NumeroComHasta = Integer.Parse(strTmp)
                        End If

                        strTmp = strTxt.Substring(79 - 1, 15)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Importe total del comprobante Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .ImporteTotal = Integer.Parse(strTmp) / 100
                        End If

                        strTmp = strTxt.Substring(94 - 1, 15)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Importe total no gravado Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .TotalNoGravado = Integer.Parse(strTmp) / 100
                        End If

                        strTmp = strTxt.Substring(109 - 1, 15)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Importe total neto gravado Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .TotalNeto = Integer.Parse(strTmp) / 100
                        End If

                        strTmp = strTxt.Substring(124 - 1, 15)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Importe liquidado IVA Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .TotalIVA = Integer.Parse(strTmp) / 100
                        End If

                        strTmp = strTxt.Substring(139 - 1, 15)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Impuesto liquidado a RNI o percepcion a no categorizados Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .TotalRNIoPercNoCat = Integer.Parse(strTmp) / 100
                        End If

                        strTmp = strTxt.Substring(154 - 1, 15)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Importe de operaciones exentas Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .TotalExento = Integer.Parse(strTmp) / 100
                        End If

                        strTmp = strTxt.Substring(184 - 1, 15)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Importe de Percepci�n Ingresos Brutos Bs.As. Inv�lido. Se esperaba un valor num�rico.")

                        Else
                            .TotalTributo02 = Integer.Parse(strTmp) / 100 'Perc.I.B. va en Tributos ahora
                        End If

                        strTmp = strTxt.Substring(290 - 1, 8)
                        .CallerId = strTmp 'Caller Id para saber quien debe imprimir esto al terminar

                        strTmp = strTxt.Substring(2 - 1, 8)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Fecha del comprobante Inv�lida. Se esperaba un valor num�rico (AAAAMMDD).")
                        ElseIf Integer.Parse(strTmp.Substring(1 - 1, 4)) > 2100 Or Integer.Parse(strTmp.Substring(1 - 1, 4)) < 2014 Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Fecha del comprobante Inv�lida. Se esperaba a�o dentro del rango 2014/2100, se encontr� " & strTmp.Substring(1 - 1, 4) & ".")
                        ElseIf Integer.Parse(strTmp.Substring(5 - 1, 2)) > 12 Or Integer.Parse(strTmp.Substring(5 - 1, 2)) < 1 Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Fecha del comprobante Inv�lida. Se esperaba mes dentro del rango 01/12, se encontr� " & strTmp.Substring(5 - 1, 2) & ".")
                        ElseIf Integer.Parse(strTmp.Substring(7 - 1, 2)) > 31 Or Integer.Parse(strTmp.Substring(7 - 1, 2)) < 1 Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Fecha del comprobante Inv�lida. Se esperaba d�a dentro del rango 01/31, se encontr� " & strTmp.Substring(7 - 1, 2) & ".")
                        Else
                            .FechaComprobante = New Date(Integer.Parse(strTmp.Substring(1 - 1, 4)), Integer.Parse(strTmp.Substring(5 - 1, 2)), Integer.Parse(strTmp.Substring(7 - 1, 2)))
                        End If

                        If .TipoComprobante = 201 Or .TipoComprobante = 206 Or .TipoComprobante = 211 Then
                            strTmp = strTxt.Substring(275 - 1, 8)
                            If Not IsNumeric(strTmp) Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Fecha del comprobante Inv�lida. Se esperaba un valor num�rico (AAAAMMDD).")
                            ElseIf Integer.Parse(strTmp.Substring(1 - 1, 4)) > 2100 Or Integer.Parse(strTmp.Substring(1 - 1, 4)) < 2014 Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Fecha del comprobante Inv�lida. Se esperaba a�o dentro del rango 2014/2100, se encontr� " & strTmp.Substring(1 - 1, 4) & ".")
                            ElseIf Integer.Parse(strTmp.Substring(5 - 1, 2)) > 12 Or Integer.Parse(strTmp.Substring(5 - 1, 2)) < 1 Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Fecha del comprobante Inv�lida. Se esperaba mes dentro del rango 01/12, se encontr� " & strTmp.Substring(5 - 1, 2) & ".")
                            ElseIf Integer.Parse(strTmp.Substring(7 - 1, 2)) > 31 Or Integer.Parse(strTmp.Substring(7 - 1, 2)) < 1 Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Fecha del comprobante Inv�lida. Se esperaba d�a dentro del rango 01/31, se encontr� " & strTmp.Substring(7 - 1, 2) & ".")
                            Else
                                .FechaVencimiento = New Date(Integer.Parse(strTmp.Substring(1 - 1, 4)), Integer.Parse(strTmp.Substring(5 - 1, 2)), Integer.Parse(strTmp.Substring(7 - 1, 2)))
                            End If

                            strTmp = strTxt.Substring(318 - 1, 22)
                            If strTmp.Length < 22 Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "CBU con formato erroneo")
                            Else
                                .CBU = strTmp
                            End If

                            strTmp = strTxt.Substring(340 - 1, 20)
                            If strTmp.Length < 1 Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Alias con formato erroneo")
                            Else
                                .CBUAlias = strTmp.TrimEnd
                            End If
                        End If

                        strTmp = strTxt.Substring(299 - 1, 4)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Alicuota de Percepci�n Ingresos Brutos Bs.As. Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .AlicuotaTributo02 = Integer.Parse(strTmp) / 100 'Perc.I.B. va en Tributos ahora
                        End If

                        strTmp = strTxt.Substring(303 - 1, 15)
                        If Not IsNumeric(strTmp) Then
                            Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Base Imponible de Percepci�n Ingresos Brutos Bs.As. Inv�lido. Se esperaba un valor num�rico.")
                        Else
                            .BaseImponibleTributo02 = Integer.Parse(strTmp) / 100 'Perc.I.B. va en Tributos ahora
                        End If

                        If .TipoComprobante = 2 Or .TipoComprobante = 3 _
                        Or .TipoComprobante = 7 Or .TipoComprobante = 8 _
                        Or .TipoComprobante = 12 Or .TipoComprobante = 13 _
                        Or .TipoComprobante = 202 Or .TipoComprobante = 203 _
                        Or .TipoComprobante = 207 Or .TipoComprobante = 208 _
                        Or .TipoComprobante = 212 Or .TipoComprobante = 213 Then
                            strTmp = strTxt.Substring(360 - 1, 8)
                            If Not IsNumeric(strTmp) Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Fecha del comprobante Inv�lida. Se esperaba un valor num�rico (AAAAMMDD).")
                            ElseIf Integer.Parse(strTmp.Substring(1 - 1, 4)) > 2100 Or Integer.Parse(strTmp.Substring(1 - 1, 4)) < 2014 Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Fecha del comprobante Inv�lida. Se esperaba a�o dentro del rango 2014/2100, se encontr� " & strTmp.Substring(1 - 1, 4) & ".")
                            ElseIf Integer.Parse(strTmp.Substring(5 - 1, 2)) > 12 Or Integer.Parse(strTmp.Substring(5 - 1, 2)) < 1 Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Fecha del comprobante Inv�lida. Se esperaba mes dentro del rango 01/12, se encontr� " & strTmp.Substring(5 - 1, 2) & ".")
                            ElseIf Integer.Parse(strTmp.Substring(7 - 1, 2)) > 31 Or Integer.Parse(strTmp.Substring(7 - 1, 2)) < 1 Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Fecha del comprobante Inv�lida. Se esperaba d�a dentro del rango 01/31, se encontr� " & strTmp.Substring(7 - 1, 2) & ".")
                            Else
                                .FechaComprobanteAsoc = New Date(Integer.Parse(strTmp.Substring(1 - 1, 4)), Integer.Parse(strTmp.Substring(5 - 1, 2)), Integer.Parse(strTmp.Substring(7 - 1, 2)))
                            End If

                            strTmp = strTxt.Substring(368 - 1, 3)
                            If Not IsNumeric(strTmp) Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Tipo de comprobante Inv�lido. Se esperaba un valor num�rico.")
                            Else
                                .TipoComprobanteAsoc = Integer.Parse(strTmp)
                            End If

                            strTmp = strTxt.Substring(371 - 1, 4)
                            If Not IsNumeric(strTmp) Then
                                Throw New Exception(
                                          "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                          "Punto de venta Inv�lido. Se esperaba un valor num�rico.")
                            Else
                                .PuntoDeVentaAsoc = Integer.Parse(strTmp)
                            End If

                            strTmp = strTxt.Substring(375 - 1, 8)
                            If Not IsNumeric(strTmp) Then
                                Throw New Exception(
                                      "Error en archivo " & strFileName.ToUpper.Replace(".TXT", "") & "." & vbCrLf &
                                      "Numero de comprobante Inv�lido. Se esperaba un valor num�rico.")
                            Else
                                .NumeroCompAsoc = Integer.Parse(strTmp)
                            End If

                        End If

                        If .TipoComprobante = 203 Or .TipoComprobante = 208 Or .TipoComprobante = 213 Then
                            strTmp = strTxt.Substring(383 - 1, 1)
                            .CodigoAnulacion = strTmp
                        End If

                        ' Generar los que no tengo en base a los que tengo
                        Dim AlicIVA As Decimal
                        If .TotalNeto > 0 Then
                            AlicIVA = Math.Round(.TotalIVA / .TotalNeto * 100, 2)
                        Else
                            AlicIVA = 0
                        End If
                        .TotalNeto03 = 0
                        .TotalNeto04 = 0
                        .TotalNeto05 = 0
                        .TotalNeto06 = 0
                        .TotalIVA03 = 0
                        .TotalIVA04 = 0
                        .TotalIVA05 = 0
                        .TotalIVA06 = 0
                        If AlicIVA = New Decimal(21) Then
                            .TotalNeto05 = .TotalNeto
                            .TotalIVA05 = .TotalIVA
                        ElseIf AlicIVA > New Decimal(10.4) And AlicIVA < New Decimal(10.6) Then
                            .TotalNeto04 = .TotalNeto
                            .TotalIVA04 = .TotalIVA
                        ElseIf AlicIVA = New Decimal(27) Then
                            .TotalNeto05 = .TotalNeto
                            .TotalIVA05 = .TotalIVA
                        Else 'If AlicIVA = New Decimal(0) Then
                            .TotalNeto03 = .TotalNeto
                            .TotalIVA03 = .TotalIVA
                        End If

                        'Tipos de Tributo (Id, Desc):
                        '1:          Impuestos(nacionales)
                        '2:          Impuestos(provinciales)=> Perc. IIBB
                        '3:          Impuestos(municipales)
                        '4:          Impuestos(Internos)
                        '99:         Otro()
                        'If ImportePercIIBB > 0 Then
                        'If .TotalNeto = 0 And .ImporteTotal = ImportePercIIBB Then
                        '   'mmm... debe ser todo no gravado, dificil que sea todo perc. i.br. x que necesita de 1 neto
                        '   .TotalNeto03 = ImportePercIIBB
                        '   .TotalIVA03 = 0
                        '   .TotalNeto = ImportePercIIBB
                        '   .BaseImponibleTributo02 = 0
                        '   .AlicuotaTributo02 = 0
                        '   .TotalTributo02 = 0
                        'Else
                        '.BaseImponibleTributo02 = .TotalNeto
                        ' If .TotalNeto <> 0 Then
                        '.AlicuotaTributo02 = Math.Round(ImportePercIIBB / .TotalNeto * 100, 2)
                        'Else
                        '.AlicuotaTributo02 = 0
                        'End If
                        '           .TotalTributo02 = ImportePercIIBB
                        '          'End If
                        '         Else
                        '        .BaseImponibleTributo02 = 0
                        '       .AlicuotaTributo02 = 0
                        '      .TotalTributo02 = 0
                        'End If
                    End With

                    LogToFile("InData" & vbCrLf & "{" &
                               "TipoDocumento: " & InData.TipoDocumento & vbCrLf &
                               "NumeroDocumento: " & InData.NumeroDocumento & vbCrLf &
                               "TipoComprobante: " & InData.TipoComprobante & vbCrLf &
                               "PuntoDeVenta: " & InData.PuntoDeVenta & vbCrLf &
                               "NumeroComDesde: " & InData.NumeroComDesde & vbCrLf &
                               "NumeroComHasta: " & InData.NumeroComHasta & vbCrLf &
                               "ImporteTotal: " & InData.ImporteTotal & vbCrLf &
                               "TotalNoGravado: " & InData.TotalNoGravado & vbCrLf &
                               "TotalNeto: " & InData.TotalNeto & vbCrLf &
                               "TotalIVA: " & InData.TotalIVA & vbCrLf &
                               "TotalRNIoPercNoCat: " & InData.TotalRNIoPercNoCat & vbCrLf &
                               "TotalExento: " & InData.TotalExento & vbCrLf &
                               "FechaComprobante: " & InData.FechaComprobante & vbCrLf &
                               "TotalNeto 03: " & InData.TotalNeto03 & vbCrLf &
                               "TotalIVA  03: " & InData.TotalIVA03 & vbCrLf &
                               "TotalNeto 04: " & InData.TotalNeto04 & vbCrLf &
                               "TotalIVA  04: " & InData.TotalIVA04 & vbCrLf &
                               "TotalNeto 05: " & InData.TotalNeto05 & vbCrLf &
                               "TotalIVA  05: " & InData.TotalIVA05 & vbCrLf &
                               "TotalNeto 06: " & InData.TotalNeto06 & vbCrLf &
                               "TotalIVA  06: " & InData.TotalIVA06 & vbCrLf &
                               "BI Tributo 01: " & InData.BaseImponibleTributo01 & vbCrLf &
                               "Alic. Trib.01: " & InData.AlicuotaTributo01 & vbCrLf &
                               "Total Trib.01: " & InData.TotalTributo01 & vbCrLf &
                               "BI Tributo 02: " & InData.BaseImponibleTributo02 & vbCrLf &
                               "Alic. Trib.02: " & InData.AlicuotaTributo02 & vbCrLf &
                               "Total Trib.02: " & InData.TotalTributo02 & vbCrLf &
                               "BI Tributo 03: " & InData.BaseImponibleTributo03 & vbCrLf &
                               "Alic. Trib.03: " & InData.AlicuotaTributo03 & vbCrLf &
                               "Total Trib.03: " & InData.TotalTributo03 & vbCrLf &
                               "BI Tributo 04: " & InData.BaseImponibleTributo04 & vbCrLf &
                               "Alic. Trib.04: " & InData.AlicuotaTributo04 & vbCrLf &
                               "Total Trib.04: " & InData.TotalTributo04 & vbCrLf &
                               "BI Tributo 99: " & InData.BaseImponibleTributo99 & vbCrLf &
                               "Alic. Trib.99: " & InData.AlicuotaTributo99 & vbCrLf &
                               "Total Trib.99: " & InData.TotalTributo99 & vbCrLf &
                               "Fecha Vencimiento: " & InData.FechaVencimiento & vbCrLf &
                               "CBU: " & InData.CBU & vbCrLf &
                               "Alias: " & InData.CBUAlias & vbCrLf &
                               "Fecha C. Asociado: " & InData.FechaComprobanteAsoc & vbCrLf &
                               "Tipo C. Asociado: " & InData.TipoComprobanteAsoc & vbCrLf &
                               "PDV C. Asociado: " & InData.PuntoDeVentaAsoc & vbCrLf &
                               "Num. C. Asociado: " & InData.NumeroCompAsoc & vbCrLf &
                               "Codigo Anulacion: " & InData.CodigoAnulacion & vbCrLf &
                               "}")
                    ParseFileV0c = True
                Catch ex As Exception
                    LogToFileAndScreen("ParseFile.Error: " & ex.Message, strFileName)
                End Try
            End If
            srdTxt.Close()
        Catch ex As Exception
            LogToFileAndScreen(ex.Message, strFileName)
        End Try
    End Function

    Private Function PasarAHistorico(ByVal strFileName As String) As Boolean
        Dim intSufijo As Integer = 0
        Dim strSufijo As String = ""
        Do Until File.Exists(AppConfig.HistoryPath & "\" & strFileName & strSufijo) = False
            strSufijo = intSufijo
            intSufijo += 1
        Loop
        File.Move(AppConfig.InPath & "\" & strFileName, AppConfig.HistoryPath & "\" & strFileName & strSufijo)
        Debug.Print(strFileName & " Pasado a Historico ")
        Estado.RefreshBandejaDeEntrada()
        Return True
    End Function

    Private Function ProcesarEnAFIP(ByVal InData As InData_t, ByRef OutData As OutData_t) As Boolean
        Dim LT As LoginTicket = New LoginTicket()
        Dim strErrorDescription As String = vbNullString
        'Chequear el ticket de login
        If Not LT.AutorizacionValidaEncontrada() = True Then
            mdlWSFEv1.ResetToken()
            If AutenticacionWSAA.ObtenerAutorizacion(strErrorDescription) = False Then
                LogToFileAndScreen(strErrorDescription, InData.FileName)
                Return False
            End If
        End If
        'Enviar la facturra.
        If Not mdlWSFEv1.Inicializado() Then
            mdlWSFEv1.Inicializar(AppConfig.CUIT)
        End If
        If mdlWSFEv1.Inicializado() Then
            Dim strCAE As String = ""
            Dim strVencimiento As String = ""
            Dim strOutReferencia As String = ""
            Dim strInReferencia As String = ""
            Dim OffsetNumeracion As Integer = 0
            Dim outErrorCode As Integer = 0

            If AppConfig.RenumerarSegunAfip = 1 Then
                Dim intNextNum As Integer = mdlWSFEv1.ObtenerProximoNumeroComprobante(InData.TipoComprobante, InData.PuntoDeVenta, InData.FileName)
                OffsetNumeracion = (InData.NumeroComDesde - intNextNum) * -1
                LogToFile(Now & " Renumerando " & mdlWSFEv1.NombreCortoComprobante(InData.TipoComprobante) & "-" & Format$(InData.PuntoDeVenta, "0000") & "-" & Format$(InData.NumeroComDesde, "00000000") & " a " & _
                                mdlWSFEv1.NombreCortoComprobante(InData.TipoComprobante) & "-" & Format$(InData.PuntoDeVenta, "0000") & "-" & Format$(InData.NumeroComDesde + OffsetNumeracion, "00000000"))
            Else
                If InData.PuntoDeVenta > 0 And InData.PuntoDeVenta < UBound(AppConfig.PuntosDeVenta) Then
                    Select Case InData.TipoComprobante
                        Case 1
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).FVA_01
                        Case 2
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).NDA_02
                        Case 3
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).NCA_03
                        Case 4
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).RCA_04
                        Case 5
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).NVA_05
                        Case 6
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).FVB_06
                        Case 7
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).NDB_07
                        Case 8
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).NCB_08
                        Case 9
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).RCB_09
                        Case 10
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).NVB_10
                        Case 39
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).OCA_39
                        Case 40
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).OCB_40
                        Case 60
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).LPA_60
                        Case 61
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).LPB_61
                        Case 63
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).LIA_63
                        Case 64
                            OffsetNumeracion = AppConfig.PuntosDeVenta(InData.PuntoDeVenta).LIB_64
                        Case Else
                            OffsetNumeracion = 0
                    End Select
                Else
                    OffsetNumeracion = 0
                End If
            End If

            OutData.ErrorCode = 0

            If mdlWSFEv1.SolicitarCAE(
                InData.FileName.Replace(".TXT", ""),
                InData.NumeroComDesde + OffsetNumeracion,
                InData.NumeroComHasta + OffsetNumeracion,
                False,
                InData.TipoDocumento,
                InData.NumeroDocumento,
                InData.TipoComprobante,
                InData.PuntoDeVenta,
                InData.ImporteTotal,
                InData.TotalNoGravado,
                InData.TotalNeto,
                InData.TotalIVA,
                InData.TotalRNIoPercNoCat,
                InData.TotalExento,
                InData.FechaComprobante,
                InData.FechaComprobante,
                InData.FechaComprobante,
                InData.FechaVencimiento,
                InData.TotalNeto03,
                InData.TotalIVA03,
                InData.TotalNeto04,
                InData.TotalIVA04,
                InData.TotalNeto05,
                InData.TotalIVA05,
                InData.TotalNeto06,
                InData.TotalIVA06,
                InData.BaseImponibleTributo01,
                InData.AlicuotaTributo01,
                InData.TotalTributo01,
                InData.BaseImponibleTributo02,
                InData.AlicuotaTributo02,
                InData.TotalTributo02,
                InData.BaseImponibleTributo03,
                InData.AlicuotaTributo03,
                InData.TotalTributo03,
                InData.BaseImponibleTributo04,
                InData.AlicuotaTributo04,
                InData.TotalTributo04,
                InData.BaseImponibleTributo99,
                InData.AlicuotaTributo99,
                InData.TotalTributo99,
                InData.CBU,
                InData.CBUAlias,
                InData.FechaComprobanteAsoc,
                InData.TipoComprobanteAsoc,
                InData.PuntoDeVentaAsoc,
                InData.NumeroCompAsoc,
                InData.CodigoAnulacion,
                strCAE,
                strVencimiento,
                strOutReferencia,
                strErrorDescription) _
                Then

                strInReferencia = Format$(InData.TipoComprobante, "00") & "-" &
                                  Format$(InData.PuntoDeVenta, "0000") & "-" &
                                  Format$(InData.NumeroComDesde, "00000000")

                If (strOutReferencia = strInReferencia) Or (AppConfig.RenumerarSegunAfip = 1) Then
                    OutData.TipoComprobante = InData.TipoComprobante
                    OutData.PuntoDeVenta = InData.PuntoDeVenta
                    OutData.NumeroComDesde = InData.NumeroComDesde
                    OutData.NumeroComHasta = InData.NumeroComHasta
                    OutData.CAE = strCAE
                    OutData.Vencimiento = strVencimiento
                    OutData.CallerId = InData.CallerId
                    Return True
                Else
                    'No coincide informacion del request con informacion del response!
                    strErrorDescription = "Error de consistencia luego de obtener CAE." & vbCrLf &
                                          "Archivo:" & InData.FileName & vbCrLf &
                                          "Detalle del Error: Se solicit� la autorizacion del comprobante: " & strInReferencia & vbCrLf &
                                          "Se obtuvo la autorizacion del comprobante: " & strOutReferencia
                    LogToFileAndScreen(strErrorDescription, InData.FileName)
                End If
            Else
                LogToFileAndScreen(strErrorDescription, InData.FileName)
                If OutData.ErrorCode = 600 Then 'ValidacionDeToken: No validaron las fechas del token GenTime, ExpTime, NowUTC: 1417519899 (12/02/2014 11:31:09 AM), 1417563159 (12/02/2014 11:32:39 PM), ahora utc : 12/03/2014 12:13:09 PM
                    LogToFile("Error 600 WSAA, se solicitara nueva autorizacion...")
                    If AutenticacionWSAA.ObtenerAutorizacion(strErrorDescription) = False Then
                        LogToFileAndScreen(strErrorDescription, InData.FileName)
                        Return False
                    End If
                    mdlWSFEv1.ResetToken()
                End If
            End If
        End If

    End Function

    Private Function PublicarCAE(ByVal InData As InData_t, ByVal OutData As OutData_t) As Boolean
        Try
            Dim strFileName As String = AppConfig.OutPath & "\" & InData.FileName
            ' = AppConfig.OutPath & "\" & _
            'Format$(OutData.PuntoDeVenta, "0000") & "-" & _
            'Format$(OutData.NumeroComprobante, "00000000") & ".TXT"

            Dim ff As System.IO.StreamWriter = _
                Microsoft.VisualBasic.FileIO.FileSystem.OpenTextFileWriter(strFileName, False, System.Text.Encoding.ASCII)
            ff.WriteLine(OutData.CAE & _
                         OutData.Vencimiento & _
                         Format$(OutData.TipoComprobante, "00") & _
                         Format$(OutData.PuntoDeVenta, "0000") & _
                         Format$(OutData.NumeroComDesde, "00000000") & _
                         Format$(OutData.NumeroComHasta, "00000000") & _
                         Left(OutData.CallerId & Space$(8), 8))
            ff.Close()
            Return True
        Catch ex As Exception
            LogToFile("Error en PublicarCAE: " & ex.Message & "; (" & OutData.ToString & ")")
            Return False
        End Try
    End Function

    Private Sub InitFswInFiles()
        With fswInFiles
            ' no incluye directorios en el monitoreo   
            .IncludeSubdirectories = False
            ' Monitoreas todos los archivos del directorio   
            .Filter = "*.*"
            ' filtros : Creaci�n, cambios en el archivo ( sin directorios )   
            .NotifyFilter = NotifyFilters.CreationTime Or _
                            NotifyFilters.Size Or _
                            NotifyFilters.FileName
            Try
                .Path = AppConfig.InPath
                .EnableRaisingEvents = True ' activa o desactiva el  FileSystemWatcher 
                'End If
                ' error   
            Catch sError As Exception
                .EnableRaisingEvents = False
                LogToFile("Main.InitMonitor.Error: " & sError.Message)
                Throw sError
            End Try
        End With
    End Sub

    Private Sub InitFswOutFiles()
        With fswOutFiles
            ' no incluye directorios en el monitoreo   
            .IncludeSubdirectories = False
            ' Monitoreas todos los archivos del directorio   
            .Filter = "*.*"
            ' filtros : Creaci�n, cambios en el archivo ( sin directorios )   
            .NotifyFilter = NotifyFilters.CreationTime Or _
                            NotifyFilters.Size Or _
                            NotifyFilters.FileName
            Try
                .Path = AppConfig.OutPath
                .EnableRaisingEvents = True ' activa o desactiva el  FileSystemWatcher 
                ' error   
            Catch sError As Exception
                .EnableRaisingEvents = False
                LogToFile("Main.InitFswOutFiles.Error: " & sError.Message)
                Throw sError
            End Try
        End With
    End Sub

    Private Sub SincronizarRelojes()
        Try
            Dim ntp As SNTPClient = New SNTPClient("time.afip.gov.ar")

            If ntp.Connect(False) Then
                Dim trts As DateTime = DateTime.Now.AddMilliseconds(ntp.LocalClockOffset)
                Dim ts As New TimeSpan(0, 0, 0, 0, ntp.LocalClockOffset)
                OffsetHoraAfip = ntp.LocalClockOffset
                LogToFile("SincronizarRelojes. ERROR FechaAFIP: " & trts & " habiendo una diferencia de " & ts.ToString())
            Else
                OffsetHoraAfip = 0
                LogToFileAndScreen(Now & "SincronizarRelojes. ERROR imposible conectar al servidor de fecha y hora de afip.", "")
            End If
        Catch ex As Exception
            OffsetHoraAfip = 0
            LogToFileAndScreen(Now & " SincronizarRelojes. ERROR al consultar srvidor de hora de red. Descripcion del Error: " & ex.Message, "")
        End Try
    End Sub

    Public Function IsConnectionAvailable() As Boolean
        ' usage: If IsConnectionAvailable() = True Then MessageBox.Show("You are online!")
        Dim objUrl As New System.Uri("http://www.google.com/")
        ' Setup WebRequest
        Dim objWebReq As System.Net.WebRequest
        objWebReq = System.Net.WebRequest.Create(objUrl)
        Dim objResp As System.Net.WebResponse = Nothing
        Try
            ' Attempt to get response and return True
            objResp = objWebReq.GetResponse
            objResp.Close()
            objWebReq = Nothing
            Return True
        Catch ex As Exception
            ' Error, exit and return False
            LogToFile(Now & "; IsConnectionAvailable Error: " & ex.Message)
            If Not objResp Is Nothing Then
                objResp.Close()
                objWebReq = Nothing
            End If
            Return False
        End Try
    End Function

    Private Sub RestartTimer()
        LogToFile(Now & "; Signaling Retry Worker Thread.")
        NextTimeout = Now.Add(New TimeSpan(0, 0, mobTimer.Interval / 1000))
        mobTimer.Stop()
        mobTimer.Start()
    End Sub

    Private Function IsValidCUIT(ByVal CUIT As String, ByVal ErrorDescription As String) As Boolean
        IsValidCUIT = True
        If Trim(CUIT) = vbNullString Or Len(CUIT) < 11 Then
            ErrorDescription = "C.U.I.T. Inv�lido"
            IsValidCUIT = True
        Else
            Dim strFactor As String, strCadena As String, intSumatoria As Integer, intCiclo As Integer, intDigitoVerificador As Integer
            strFactor = "5432765432"
            intSumatoria = 0
            If InStr(1, CUIT, "-") = 0 Then
                strCadena = CUIT
            Else
                strCadena = Left$(CUIT, 2) & Mid$(CUIT, 4, 8) '& Right$(CUIT, 1)
            End If
            If Val(strCadena) = 0 Then
                ErrorDescription = "C.U.I.T. Inv�lido"
                IsValidCUIT = True
                Exit Function
            End If
            For intCiclo = 1 To 10
                intSumatoria = intSumatoria + (Val(Mid$(strCadena, intCiclo, 1)) * Val(Mid$(strFactor, intCiclo, 1)))
            Next intCiclo
            intDigitoVerificador = 11 - (intSumatoria Mod 11)
            Select Case intDigitoVerificador
                Case 10
                    intDigitoVerificador = 9
                Case 11
                    intDigitoVerificador = 0
            End Select
            If Val(Right$(CUIT, 1)) <> intDigitoVerificador Then
                ErrorDescription = "C.U.I.T. No Coincidente Con D�gito Verificador"
                IsValidCUIT = True
            End If
        End If
    End Function

End Module
