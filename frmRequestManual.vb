Public Class frmRequestManual

    Private Sub frmRequestManual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With Me
            .id.Text = "0"
            .presta_serv.Checked = False
            .tipo_doc.Text = "80"
            .nro_doc.Text = "20132111619"
            .tipo_cbte.Text = "1"
            .punto_vta.Text = "0011"
            .cbt_numero.Text = "00000001"
            .imp_total.Text = "121.00"
            .imp_tot_conc.Text = "0.00"
            .imp_neto.Text = "100.00"
            .impto_liq.Text = "21.00"
            .impto_liq_rni.Text = "0.00"
            .imp_op_ex.Text = "0.00"
            .fecha_cbte.Text = Today
            .fecha_serv_desde.Text = Today
            .fecha_serv_hasta.Text = Today
            .fecha_venc_pago.Text = Today
        End With
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim strCAE As String = ""
        Dim strVencimiento As String = ""
        Dim strErrorDescription As String = ""

        Dim strRta As String = mdlWSFE.SolicitarCAEManual( _
        Me.id.Text, _
        Me.presta_serv.Checked, _
        Me.cbt_numero.Text, _
        Me.cbt_numero.Text, _
        Me.tipo_doc.Text, _
        Me.nro_doc.Text, _
        Me.tipo_cbte.Text, _
        Me.punto_vta.Text, _
        Me.imp_total.Text, _
        Me.imp_tot_conc.Text, _
        Me.imp_neto.Text, _
        Me.impto_liq.Text, _
        Me.impto_liq_rni.Text, _
        Me.imp_op_ex.Text, _
        Me.fecha_cbte.Text, _
        Me.fecha_serv_desde.Text, _
        Me.fecha_serv_hasta.Text, _
        Me.fecha_venc_pago.Text, _
        strCAE, strVencimiento, strErrorDescription)

        Me.txtResult.Text = strRta & ""
        If strErrorDescription <> "" Then
            MsgBox(strErrorDescription, MsgBoxStyle.Critical, "ERROR")
        End If
        If strCAE <> "" Then
            Me.txtCAE.Text = strCAE
        End If
        If strVencimiento <> "" Then
            Me.txtVTO.Text = strVencimiento
        End If
    End Sub
End Class