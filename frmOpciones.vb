Imports IEFacturaElectronicaTray.InternetTime

Public Class frmOpciones

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        'Validar
        'Aplicar cambios
        With AppConfig
            .InPath = Me.txtInFolder.Text
            .OutPath = Me.txtOutPath.Text
            .HistoryPath = Me.txtHistorytPath.Text
            .ErrorPath = Me.txtErrPath.Text
            .TempPath = Me.txtTmpPath.Text
            .NotifyTo = Me.txtNotifyTo.Text
            .Host = Me.txtHost.Text
            .Puerto = Me.txtPuerto.Text
            .User = Me.txtUser.Text
            .Password = Me.txtPassword.Text
            .RetryInterval = Me.txtRetry.Text
            '.CUIT = Me.txtCUIT.Text
        End With
        If mdlIEFEMain.fswInFiles.Path <> AppConfig.InPath Then
            mdlIEFEMain.fswInFiles.Path = AppConfig.InPath
        End If
        If mdlIEFEMain.fswOutFiles.Path <> AppConfig.OutPath Then
            mdlIEFEMain.fswOutFiles.Path = AppConfig.OutPath
        End If
        'guardar en archivo
        Try
            Dim ff As System.IO.StreamWriter = Microsoft.VisualBasic.FileIO.FileSystem.OpenTextFileWriter(My.Application.Info.DirectoryPath & "\AppConfig.txt", False)
            With AppConfig
                ff.WriteLine("In Path=" & .InPath)
                ff.WriteLine("Out Path=" & .OutPath)
                ff.WriteLine("History Path=" & .HistoryPath)
                ff.WriteLine("CUIT=" & .CUIT)
                ff.WriteLine("Offset Numeracion=" & .OffsetNumeracion)
                ff.WriteLine("Mail Host=" & .Host)
                ff.WriteLine("Mail Puerto=" & .Puerto)
                ff.WriteLine("Mail User=" & .User)
                ff.WriteLine("Mail Password=" & .Password)
                ff.WriteLine("RetryInterval=" & .RetryInterval)
                ff.WriteLine("En error notificar a=" & .NotifyTo)
                ff.WriteLine("Renumerar segun AFIP=" & .RenumerarSegunAfip)
                Dim i As Integer
                For i = 0 To 9
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").FVA_01=" & .PuntosDeVenta(i).FVA_01)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").NDA_02=" & .PuntosDeVenta(i).NDA_02)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").NCA_03=" & .PuntosDeVenta(i).NCA_03)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").RCA_04=" & .PuntosDeVenta(i).RCA_04)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").NVA_05=" & .PuntosDeVenta(i).NVA_05)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").FVB_06=" & .PuntosDeVenta(i).FVB_06)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").NDB_07=" & .PuntosDeVenta(i).NDB_07)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").NCB_08=" & .PuntosDeVenta(i).NCB_08)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").RCB_09=" & .PuntosDeVenta(i).RCB_09)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").NVB_10=" & .PuntosDeVenta(i).NVB_10)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").OCA_39=" & .PuntosDeVenta(i).OCA_39)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").OCB_40=" & .PuntosDeVenta(i).OCB_40)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").LPA_60=" & .PuntosDeVenta(i).LPA_60)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").LPB_61=" & .PuntosDeVenta(i).LPB_61)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").LIA_63=" & .PuntosDeVenta(i).LIA_63)
                    ff.WriteLine("PDV(" & Format$(i, "00") & ").LIB_64=" & .PuntosDeVenta(i).LIB_64)
                Next
            End With

            ff.Close()
        Catch obEx As Exception
            Throw obEx
        End Try

        Me.Close()
    End Sub

    Private Sub frmOpciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With AppConfig
            Me.txtInFolder.Text = .InPath
            Me.txtOutPath.Text = .OutPath
            Me.txtHistorytPath.Text = .HistoryPath
            Me.txtNotifyTo.Text = .NotifyTo
            Me.txtHost.Text = .Host
            Me.txtPuerto.Text = .Puerto
            Me.txtUser.Text = .User
            Me.txtPassword.Text = .Password
            Me.txtRetry.Text = .RetryInterval
            'Me.txtCUIT.Text = .CUIT
        End With
        Me.txtCOM.Text = "01"
        Me.txtPDV.Text = "0002"
        Me.Text = "Opciones - Facturaci�n Electr�nica V." & mdlGetVersion() & " - OSAM"
    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Dim intNextNum As Integer = mdlWSFEv1.ObtenerProximoNumeroComprobante(Me.txtCOM.Text, Int(Me.txtPDV.Text), "")
        MsgBox("Comprobante Tipo: " & Me.txtCOM.Text & vbCrLf & _
               "Punto de Venta: " & Me.txtPDV.Text & vbCrLf & _
               "N�mero esperado: " & intNextNum)
    End Sub

    Private Sub btnFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFecha.Click
        Try
            Dim ntp As SNTPClient = New SNTPClient("time.afip.gov.ar")

            If ntp.Connect(False) Then
                Dim trts As DateTime = DateTime.Now.AddMilliseconds(ntp.LocalClockOffset)
                Dim ts As New TimeSpan(0, 0, 0, 0, ntp.LocalClockOffset)
                Me.txtFecha.Text = trts
                Me.txtDifFecha.Text = ts.ToString()
            Else
                LogToFileAndScreen(Now & " ERROR imposible conectar al servidor de fecha y hora de afip.", "")
            End If

        Catch ex As Exception
            LogToFileAndScreen(Now & " ERROR al consultar srvidor de hora de red. Descripcion del Error: " & ex.Message, "")
        End Try
    End Sub

    Private Sub btnDummy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDummy.Click
        Dim strRet As String = mdlWSFEv1.CallDummy()
        MsgBox(strRet)
    End Sub

    Private Sub btnConsultaCAE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultaCAE.Click
        Try
            Dim strRet As String = mdlWSFEv1.ConsultarCAE(CInt(Me.txtQueryCAE_TipoCom.Text), CInt(Me.txtQueryCAE_Pdv.Text), CInt(Me.txtQueryCAE_Numero.Text))
            LogToFile("ConsultarCAE:" & vbCrLf & strRet)
            MsgBox(strRet)
        Catch ex As Exception
            LogToFile(Now() & " ConsultarCAE.Error = (" & Err.Number & ") " & ex.Message)
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "ERROR")
        End Try
    End Sub

    Private Sub btnEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmail.Click
        Try
            Dim email As New SendMail(AppConfig.Host, AppConfig.Puerto, AppConfig.User, AppConfig.Password)
            Dim ToList() As String = AppConfig.NotifyTo.Split(";")
            Dim strErrorDescription As String = ""
            If InData.FileName Is Nothing Then InData.FileName = ""
            LogToFile("email.SendEmailMessage('cae@osam.org.ar', '" & AppConfig.NotifyTo & "')")
            email.SendEmailMessage("cae@osam.org.ar", _
                    ToList, _
                    "OSAM - TEST ENVIO EMAIL", _
                    "Mensaje autogenerado para probar el env�o de correo electr�nico de notificaci�n.", _
                    strErrorDescription)
            If strErrorDescription <> "" Then
                LogToFile("NonModalMessageBox.email.Error: " & strErrorDescription)
            End If
        Catch ex As Exception
            LogToFile("NonModalMessageBox.sendmail.Error: " & ex.Message)
        End Try
    End Sub
End Class