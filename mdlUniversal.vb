Module mdlUniversal

    Private MsgText       As String
    Private FileWithError As String
    Public ErrorList As List(Of String) = New List(Of String)

    Public Sub LogToFile(ByVal Texto As String)
        Try
            Dim ff As System.IO.StreamWriter = Microsoft.VisualBasic.FileIO.FileSystem.OpenTextFileWriter(My.Application.Info.DirectoryPath & "\Log.txt", True, System.Text.Encoding.ASCII)
            ff.WriteLine(Texto)
            ff.Close()
            'Console.WriteLine(Texto)
        Catch
        End Try
    End Sub

    Public Sub LogToFileAndScreen(ByVal Texto As String, ByVal ArchivoAfectado As String)
        Try
            LogToFile(Texto)
            ErrorList.Add(Texto)
            'mobNotifyIcon.ShowBalloonTip(5000, "ERROR", Texto, ToolTipIcon.Info)
            'MsgBox(Texto, MsgBoxStyle.Exclamation, "ERROR")
            MsgText = Texto
            FileWithError = ArchivoAfectado
            Dim NT As New System.Threading.Thread(AddressOf NonModalMessageBox)
            NT.Start()
            NT = Nothing
        Catch
        End Try
    End Sub

    Private Sub NonModalMessageBox()
        Try
            Dim email As New SendMail(AppConfig.Host, AppConfig.Puerto, AppConfig.User, AppConfig.Password)
            Dim ToList() As String = AppConfig.NotifyTo.Split(";")
            Dim strErrorDescription As String = ""
            If InData.FileName Is Nothing Then InData.FileName = ""
            email.SendEmailMessage("cae@osam.org.ar", ToList, "OSAM - ERROR CAE " & FileWithError & "", MsgText, strErrorDescription)
            If strErrorDescription <> "" Then
                LogToFile("NonModalMessageBox.email.Error: " & strErrorDescription)
            End If
        Catch ex As System.Exception
            LogToFile("NonModalMessageBox.sendmail.Error: " & ex.Message)
        End Try
        'MessageBox.Show(MsgText, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    End Sub

    Public Function mdlGetVersion() As String
        '1.0.3  2014/12/03: manejo de error 600
        '1.0.4  2014/12/09: mdlWSFEv1.ResetToken() era el motivo del err.600
        '1.0.5  2015/08/11: bugfix Today
        '1.0.6  2015/09/08: reemplazo el fsw por un fucking raw timer debido a que a veces no se ejecutan los eventos del file system.
        '1.0.7  2015/10/29: lvwErr y DoNotUpdateUI
        '1.0.8  2017/01/02: no incluir registros de iva si es comprobante factura C
        '1.0.9  2017/03/30: campos extra para percepcion iibb
        '1.0.10 2019/10/04: Factura de credito electronica MiPymes
        '1.0.11 2019/11/22: Nota de Credito/Debito electronico MiPymes
        '1.0.12 2021/04/11: Res. Circulacion abierta y comprobante asociado
        mdlGetVersion = "1.0.12"
    End Function

End Module

