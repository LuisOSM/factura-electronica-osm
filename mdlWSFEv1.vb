Imports System.Xml
Imports System.Xml.Serialization
Imports System.Xml.XmlException
Imports System.IO
Imports Microsoft.VisualBasic.FileIO.FileSystem

Module mdlWSFEv1
    Private objWSFEv1        As New wsfev1.Service
    Private objFEAuthRequest As New wsfev1.FEAuthRequest
    Private RutaTicketAcceso As String = My.Application.Info.DirectoryPath & "\WSAA_TA.xml" ' Debe indicar la Ruta de su Ticket de acceso
    Private CUIT             As String               ' Debe indicar la CUIT del emisor de los comprobantes
    Private Token            As String
    Private Sign             As String
    Private CAE              As String

    Public Structure SolicitudCAEDetalle
        Public FileName               As String
        Public NumeroDesde            As Integer
        Public NumeroHasta            As Integer
        Public EsServicio             As Boolean
        Public TipoDoc                As Integer
        Public NroDoc                 As String
        Public TipoComprobante        As Integer
        Public PuntoDeVenta           As Integer
        Public ImporteTotal           As Decimal
        Public ImporteNoGravado       As Decimal
        Public ImporteNeto            As Decimal
        Public ImporteIVA             As Decimal
        Public ImporteRNI             As Decimal
        Public ImporteEXE             As Decimal
        Public FechaComprobante       As Date
        Public FechaServDesde         As Date
        Public FechaServHasta         As Date
        Public FechaVencimiento       As Date
        Public ImporteNeto03          As Decimal
        Public ImporteIVA03           As Decimal
        Public ImporteNeto04          As Decimal
        Public ImporteIVA04           As Decimal
        Public ImporteNeto05          As Decimal
        Public ImporteIVA05           As Decimal
        Public ImporteNeto06          As Decimal
        Public ImporteIVA06           As Decimal
        Public BaseImponibleTributo01 As Decimal
        Public AlicuotaTributo01      As Decimal
        Public ImporteTributo01       As Decimal
        Public BaseImponibleTributo02 As Decimal
        Public AlicuotaTributo02      As Decimal
        Public ImporteTributo02       As Decimal
        Public BaseImponibleTributo03 As Decimal
        Public AlicuotaTributo03      As Decimal
        Public ImporteTributo03       As Decimal
        Public BaseImponibleTributo04 As Decimal
        Public AlicuotaTributo04      As Decimal
        Public ImporteTributo04       As Decimal
        Public BaseImponibleTributo99 As Decimal
        Public AlicuotaTributo99      As Decimal
        Public ImporteTributo99       As Decimal
        Public outCAE                 As String
        Public outVencimiento         As String
        Public outReferencia          As String
        Public outErrorDescription    As String
    End Structure

    Public Function Inicializado() As Boolean
        If CUIT = vbNullString Or Token = vbNullString Or Sign = vbNullString Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function Inicializar(ByVal strCUIT As String) As Boolean
        objWSFEv1.Url = "https://servicios1.afip.gov.ar/wsfev1/service.asmx?WSDL"
        'objWSFEv1.Url = "https://wswhomo.afip.gov.ar/wsfev1/service.asmx?WSDL"
        objWSFEv1.Timeout = 60 * 2 * 1000
        CUIT = strCUIT
        ' Obtengo del tikcet de acceso los campos token y sign
        If FileExists(RutaTicketAcceso) Then
            Dim m_xmld As XmlDocument
            Dim m_nodelist As XmlNodeList
            Dim m_node As XmlNode
            m_xmld = New XmlDocument()
            m_xmld.Load(RutaTicketAcceso)
            m_nodelist = m_xmld.SelectNodes("/loginTicketResponse/credentials")
            For Each m_node In m_nodelist
                Token = m_node.ChildNodes.Item(0).InnerText
                Sign = m_node.ChildNodes.Item(1).InnerText
            Next
            CAE = vbNullString
            Return True
        Else
            LogToFile("Error en mdlWSFEv1.Inicializar: No se encontr� el ticket de acceso (Archivo: " + RutaTicketAcceso)
        End If
    End Function

    Private Function ObtenerProximoID(ByRef Id As Integer, ByRef ErrorDescription As String, ByVal FileName As String) As Integer
        Return 0 'Ya no se usa el id
    End Function

    Public Function ObtenerProximoNumeroComprobante(ByVal TipoCbte As Integer, ByVal PtoVta As Integer, ByVal FileName As String) As Integer
        'Con el m�todo FERecuperaLastCMPRequest puedo determinar el numero de comprobante que
        'debieramos estar procesando en este momento.
        'nos serviria para llenar FEDetalleRequest.cbt_desde y FEDetalleRequest.cbt_hasta

        Dim objFERecuperaLastCMPResponse As New wsfev1.FERecuperaLastCbteResponse

        If Inicializado() = False Then
            Dim strErrorDescription As String = ""
            If AutenticacionWSAA.ObtenerAutorizacion(strErrorDescription) = True Then
                Inicializar(AppConfig.CUIT) 'Para refrescar el Token y Sign
                If Inicializado() = False Then
                    LogToFile(Now & " Token y Sign no inicializados... seguramente habra un error en ObtenerProximoNumeroComprobante")
                End If
            Else
                LogToFile(Now & strErrorDescription)
            End If
        End If
        objFEAuthRequest.Cuit = mdlWSFEv1.CUIT 'validar
        objFEAuthRequest.Token = mdlWSFEv1.Token 'validar
        objFEAuthRequest.Sign = mdlWSFEv1.Sign 'validar
        Try
            LogToFile(Now & " Call FECompUltimoAutorizado")
            objFERecuperaLastCMPResponse = objWSFEv1.FECompUltimoAutorizado(objFEAuthRequest, PtoVta, TipoCbte)
            LogToFile(Now & " Returned from FECompUltimoAutorizado")
            If objFERecuperaLastCMPResponse Is Nothing Then
                LogToFileAndScreen(Now & " Error logico al ObtenerProximoNumeroComprobante(): " & "El servidor de AFIP devolvi� una respuesta nula.", FileName)
            ElseIf Not objFERecuperaLastCMPResponse.Errors Is Nothing Then
                Dim strErrors As String = ""
                For Each e As wsfev1.Err In objFERecuperaLastCMPResponse.Errors
                    strErrors &= "(" & e.Code & ") " & e.Msg & vbCrLf
                Next
                LogToFileAndScreen(Now & " Error logico al ObtenerProximoNumeroComprobante(): " & strErrors, FileName)
            End If
            Return objFERecuperaLastCMPResponse.CbteNro + 1
        Catch ex As TimeoutException
            LogToFile("Error en mdlWSFEv1.ObtenerProximoNumeroComprobante: " & ex.Message)
            Return False
        Catch ex As Exception
            LogToFile("Error en mdlWSFEv1.ObtenerProximoNumeroComprobante: " & ex.Message)
        End Try
    End Function

    Private Function DescripcionMotivo(ByVal strCodigo As String) As String
        Select Case strCodigo
            Case "10001" : Return "La cantidad de registros de detalle de la factura/lote de ingreso debe ser mayor que 0." '<CantReg>
            Case "10002" : Return "La cantidad de registros de detalle de la factura/lote de ingreso debe ser igual a lo informado en cabecera de la factura/lote de ingreso." '<CantReg>
            Case "10003" : Return "Cantidad de registros en detalle mayor a valor permitido." ' Consulte m�todo FECAERegTotXRequest para obtener cantidad m�xima de registros por cada requerimiento
            Case "10004" : Return "El punto de venta debe estar comprendido entre 1 y 9998."
            Case "10005" : Return "El punto de venta debe estar habilitado para r�gimen RECE"
            Case "10006" : Return "El tipo de comprobante debe estar comprendido entre 1 y 999"
            Case Else
                Return "CODIGO AFIP " & strCodigo
        End Select
    End Function

    Public Function NombreCortoComprobante(ByVal CodigoComprobante As Integer) As String
        Select Case CodigoComprobante
            Case 1
                Return "FV-A"
            Case 2
                Return "ND-A"
            Case 3
                Return "NC-A"
            Case 4
                Return "RC-A"
            Case 5
                Return "NV-A"
            Case 6
                Return "FV-B"
            Case 7
                Return "ND-B"
            Case 8
                Return "NC-B"
            Case 9
                Return "RC-B"
            Case 10
                Return "NV-B"
            Case 39
                Return "OC-A"
            Case 40
                Return "OC-B"
            Case 60
                Return "LP-A"
            Case 61
                Return "LP-B"
            Case 63
                Return "LI-A"
            Case 64
                Return "LI-B"
            Case Else
                Return "NN-N"
        End Select

    End Function

    Public Function SolicitarCAE(
        ByVal FileName As String,
        ByVal NumeroDesde As Integer,
        ByVal NumeroHasta As Integer,
        ByVal EsServicio As Boolean,
        ByVal TipoDoc As Integer,
        ByVal NroDoc As String,
        ByVal TipoComprobante As Integer,
        ByVal PuntoDeVenta As Integer,
        ByVal ImporteTotal As Decimal,
        ByVal ImporteNoGravado As Decimal,
        ByVal ImporteNeto As Decimal,
        ByVal ImporteIVA As Decimal,
        ByVal ImporteRNI As Decimal,
        ByVal ImporteEXE As Decimal,
        ByVal FechaComprobante As Date,
        ByVal FechaServDesde As Date,
        ByVal FechaServHasta As Date,
        ByVal FechaVencimiento As Date,
        ByVal ImporteNeto03 As Decimal,
        ByVal ImporteIVA03 As Decimal,
        ByVal ImporteNeto04 As Decimal,
        ByVal ImporteIVA04 As Decimal,
        ByVal ImporteNeto05 As Decimal,
        ByVal ImporteIVA05 As Decimal,
        ByVal ImporteNeto06 As Decimal,
        ByVal ImporteIVA06 As Decimal,
        ByVal BaseImponibleTributo01 As Decimal,
        ByVal AlicuotaTributo01 As Decimal,
        ByVal ImporteTributo01 As Decimal,
        ByVal BaseImponibleTributo02 As Decimal,
        ByVal AlicuotaTributo02 As Decimal,
        ByVal ImporteTributo02 As Decimal,
        ByVal BaseImponibleTributo03 As Decimal,
        ByVal AlicuotaTributo03 As Decimal,
        ByVal ImporteTributo03 As Decimal,
        ByVal BaseImponibleTributo04 As Decimal,
        ByVal AlicuotaTributo04 As Decimal,
        ByVal ImporteTributo04 As Decimal,
        ByVal BaseImponibleTributo99 As Decimal,
        ByVal AlicuotaTributo99 As Decimal,
        ByVal ImporteTributo99 As Decimal,
        ByVal CBU As String,
        ByVal CBUAlias As String,
        ByVal FechaComprobanteAsoc As Date,
        ByVal TipoComprobanteAsoc As Integer,
        ByVal PuntoDeVentaAsoc As Integer,
        ByVal NumeroCompAsoc As Integer,
        ByVal CodigoAnulacion As String,
        ByRef outCAE As String,
        ByRef outVencimiento As String,
        ByRef outReferencia As String,
        ByRef outErrorDescription As String
    ) As Boolean

        Dim objFECAERequest As New wsfev1.FECAERequest
        Dim objFECAECabRequest As New wsfev1.FECAECabRequest
        Dim ArrayOfFECAEDetRequest(0) As wsfev1.FECAEDetRequest
        Dim objFECAEResponse As New wsfev1.FECAEResponse

        If mdlWSFEv1.CUIT = vbNullString Then
            LogToFile("mdlWSFEv1.SolicitarCAE() CUIT no inicializado.")
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf &
                                  "Archivo:" & FileName & vbCrLf &
                                  "Detalle del Error: CUIT no inicializado."
            OutData.ErrorCode = -1
            Return False
        End If
        If mdlWSFEv1.Token = vbNullString Then
            LogToFile("mdlWSFEv1.SolicitarCAE() Token no inicializado.")
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf &
                                  "Archivo:" & FileName & vbCrLf &
                                  "Detalle del Error: Token no inicializado."
            OutData.ErrorCode = -2
            Return False
        End If
        If mdlWSFEv1.Sign = vbNullString Then
            LogToFile("mdlWSFEv1.SolicitarCAE() Sign no inicializado.")
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf &
                                  "Archivo:" & FileName & vbCrLf &
                                  "Detalle del Error: Sign no inicializado."
            OutData.ErrorCode = -3
            Return False
        End If

        objFEAuthRequest.Cuit = mdlWSFEv1.CUIT
        objFEAuthRequest.Token = mdlWSFEv1.Token
        objFEAuthRequest.Sign = mdlWSFEv1.Sign

        Dim strErrorDescription As String = ""
        'Dim intId As Integer
        'If ObtenerProximoID(intId, strErrorDescription) = False Then
        '   outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & _
        '                         "Archivo:" & FileName & vbCrLf & _
        '                         "Detalle del Error: Error al obtener proximo Id, " & vbCrLf & _
        '                         strErrorDescription
        '   Return False
        'End If
        With objFECAECabRequest
            .CantReg = 1
            .CbteTipo = TipoComprobante
            .PtoVta = PuntoDeVenta
        End With
        objFECAERequest.FeCabReq = objFECAECabRequest

        outCAE = ""
        outVencimiento = ""

        'Detalles del comprobante
        ArrayOfFECAEDetRequest(0) = New wsfev1.FECAEDetRequest
        With ArrayOfFECAEDetRequest(0) 'validar todo
            .Concepto = 1 'PRODUCTOS
            .DocTipo = TipoDoc
            .DocNro = NroDoc
            .CbteDesde = NumeroDesde
            .CbteHasta = NumeroHasta
            'FechaComprobante = Date.Today
            .CbteFch = Format$(FechaComprobante, "yyyyMMdd")
            .ImpTotal = ImporteTotal
            .ImpTotConc = ImporteNoGravado 'No Gravado
            .ImpNeto = ImporteNeto
            .ImpOpEx = ImporteEXE
            .ImpTrib = ImporteTributo01 + ImporteTributo02 + ImporteTributo03 + ImporteTributo04 + ImporteTributo99 'Perc. II BB
            .ImpIVA = ImporteIVA
            If TipoComprobante = 201 Or TipoComprobante = 206 Or TipoComprobante = 211 Then
                .FchVtoPago = Format$(FechaVencimiento, "yyyyMMdd")
            End If
            If .Concepto = 2 Or .Concepto = 3 Then
                .FchVtoPago = Format$(FechaVencimiento, "yyyyMMdd")
                .FchServDesde = Format$(FechaServDesde, "yyyyMMdd")
                .FchServHasta = Format$(FechaServHasta, "yyyyMMdd")
            End If
            .MonId = "PES"
            .MonCotiz = 1

            'OPCIONALES
            If TipoComprobante = 201 Or TipoComprobante = 206 Or TipoComprobante = 211 Then
                Dim ArrayOfOpcional(1) As wsfev1.Opcional
                Dim intOIndex As Integer = 0

                ArrayOfOpcional(intOIndex) = New wsfev1.Opcional
                With ArrayOfOpcional(intOIndex)
                    .Id = "2101"
                    .Valor = CBU
                End With

                intOIndex += 1

                ArrayOfOpcional(intOIndex) = New wsfev1.Opcional
                With ArrayOfOpcional(intOIndex)
                    .Id = "2102"
                    .Valor = CBUAlias
                End With

                ArrayOfOpcional(intOIndex) = New wsfev1.Opcional
                With ArrayOfOpcional(intOIndex)
                    .Id = "27"
                    .Valor = "SCA"
                End With

                .Opcionales = ArrayOfOpcional

            ElseIf TipoComprobante = 203 Or TipoComprobante = 208 Or TipoComprobante = 213 Then

                Dim ArrayOfOpcional(0) As wsfev1.Opcional
                ArrayOfOpcional(0) = New wsfev1.Opcional
                With ArrayOfOpcional(0)
                    .Id = "22"
                    .Valor = CodigoAnulacion
                End With

                .Opcionales = ArrayOfOpcional

            End If

            If TipoComprobante = 2 Or TipoComprobante = 3 _
            Or TipoComprobante = 7 Or TipoComprobante = 8 _
            Or TipoComprobante = 12 Or TipoComprobante = 13 _
            Or TipoComprobante = 202 Or TipoComprobante = 203 _
            Or TipoComprobante = 207 Or TipoComprobante = 208 _
            Or TipoComprobante = 212 Or TipoComprobante = 213 Then
                Dim ArrayOfCbteAsoc(0) As wsfev1.CbteAsoc
                ArrayOfCbteAsoc(0) = New wsfev1.CbteAsoc

                With ArrayOfCbteAsoc(0)
                    .Cuit = CUIT
                    .CbteFch = Format$(FechaComprobanteAsoc, "yyyyMMdd")
                    .Nro = NumeroCompAsoc
                    .PtoVta = PuntoDeVentaAsoc
                    .Tipo = TipoComprobanteAsoc
                End With

                .CbtesAsoc = ArrayOfCbteAsoc
            End If

            'Tipos de Tributo (Id, Desc):
            '1:          Impuestos(nacionales)
            '2:          Impuestos(provinciales)=> Perc. IIBB
            '3:          Impuestos(municipales)
            '4:          Impuestos(Internos)
            '99:         Otro()
            'ArrayOfTributos(0) = New wsfev1.Tributo
            'With ArrayOfTributos(0)
            '    .Id = 2
            '    .Desc = "Percepcion Ingresos Brutos Buenos Aires"
            '    .BaseImp = ImporteNeto
            '    .Alic = Math.Round(ImporteNoGravado / ImporteNeto * 100, 2)
            '    .Importe = ImporteNoGravado
            'End With
            '.Tributos = ArrayOfTributos
            '--- TRIBUTOS
            Dim intCantidadTributos As Integer = 0
            If BaseImponibleTributo01 <> 0 Then
                intCantidadTributos += 1
            End If
            If BaseImponibleTributo02 <> 0 Then
                intCantidadTributos += 1
            End If
            If BaseImponibleTributo03 <> 0 Then
                intCantidadTributos += 1
            End If
            If BaseImponibleTributo04 <> 0 Then
                intCantidadTributos += 1
            End If
            If BaseImponibleTributo99 <> 0 Then
                intCantidadTributos += 1
            End If
            If intCantidadTributos > 0 Then
                Dim ArrayOfTributos(intCantidadTributos - 1) As wsfev1.Tributo
                Dim intTIndex As Integer = 0
                If BaseImponibleTributo01 <> 0 Then
                    ArrayOfTributos(intTIndex) = New wsfev1.Tributo
                    With ArrayOfTributos(intTIndex)
                        .Id = 1 'IMPUESTOS NACIONALES
                        .Desc = "IMPUESTOS NACIONALES"
                        .BaseImp = BaseImponibleTributo01
                        .Alic = AlicuotaTributo01
                        .Importe = ImporteTributo01
                    End With
                    intTIndex += 1
                End If
                If BaseImponibleTributo02 <> 0 Then
                    ArrayOfTributos(intTIndex) = New wsfev1.Tributo
                    With ArrayOfTributos(intTIndex)
                        .Id = 2 'IMPUESTOS PROVINCIALES
                        .Desc = "Percepcion Ingresos Brutos Buenos Aires"
                        .BaseImp = BaseImponibleTributo02
                        .Alic = AlicuotaTributo02
                        .Importe = ImporteTributo02
                    End With
                    intTIndex += 1
                End If
                If BaseImponibleTributo03 <> 0 Then
                    ArrayOfTributos(intTIndex) = New wsfev1.Tributo
                    With ArrayOfTributos(intTIndex)
                        .Id = 3 'IMPUESTOS MUNICIPALES
                        .Desc = "IMPUESTOS MUNICIPALES"
                        .BaseImp = BaseImponibleTributo03
                        .Alic = AlicuotaTributo03
                        .Importe = ImporteTributo03
                    End With
                    intTIndex += 1
                End If
                If BaseImponibleTributo04 <> 0 Then
                    ArrayOfTributos(intTIndex) = New wsfev1.Tributo
                    With ArrayOfTributos(intTIndex)
                        .Id = 4 'IMPUESTOS INTERNOS
                        .Desc = "IMPUESTOS INTERNOS"
                        .BaseImp = BaseImponibleTributo04
                        .Alic = AlicuotaTributo04
                        .Importe = ImporteTributo04
                    End With
                    intTIndex += 1
                End If
                If BaseImponibleTributo99 <> 0 Then
                    ArrayOfTributos(intTIndex) = New wsfev1.Tributo
                    With ArrayOfTributos(intTIndex)
                        .Id = 99 'IMPUESTOS VARIOS
                        .Desc = "IMPUESTOS VARIOS"
                        .BaseImp = BaseImponibleTributo99
                        .Alic = AlicuotaTributo99
                        .Importe = ImporteTributo99
                    End With
                    intTIndex += 1
                End If
                .Tributos = ArrayOfTributos
            End If

            '--- IVA
            Dim intCantidadAlicuotas As Integer = 0
            Dim blnForzar As Boolean = False
            If ImporteNeto03 <> 0 Then
                intCantidadAlicuotas += 1
            End If
            If ImporteNeto04 <> 0 Then
                intCantidadAlicuotas += 1
            End If
            If ImporteNeto05 <> 0 Then
                intCantidadAlicuotas += 1
            End If
            If ImporteNeto06 <> 0 Then
                intCantidadAlicuotas += 1
            End If
            'If intCantidadAlicuotas = 0 And ImporteTotal > 0 Then
            '    intCantidadAlicuotas = 1
            '    blnForzar = True
            'End If
            If intCantidadAlicuotas > 0 And
               TipoComprobante <> 11 And
               TipoComprobante <> 12 And
               TipoComprobante <> 13 And
               TipoComprobante <> 211 And
               TipoComprobante <> 212 And
               TipoComprobante <> 213 _
               Then
                Dim ArrayOfIVA(intCantidadAlicuotas - 1) As wsfev1.AlicIva
                '3	 0.00%
                '4	10.50%
                '5	21.00%
                '6	27.00%
                Dim intIndex As Integer = 0
                If ImporteNeto03 <> 0 Or blnForzar Then
                    ArrayOfIVA(intIndex) = New wsfev1.AlicIva
                    With ArrayOfIVA(intIndex)
                        .Id = 3 '0.00%
                        .BaseImp = ImporteNeto03
                        .Importe = ImporteIVA03
                    End With
                    intIndex += 1
                End If
                If ImporteNeto04 <> 0 Then
                    ArrayOfIVA(intIndex) = New wsfev1.AlicIva
                    With ArrayOfIVA(intIndex)
                        .Id = 4 '10.50%
                        .BaseImp = ImporteNeto04
                        .Importe = ImporteIVA04
                    End With
                    intIndex += 1
                End If
                If ImporteNeto05 <> 0 Then
                    ArrayOfIVA(intIndex) = New wsfev1.AlicIva
                    With ArrayOfIVA(intIndex)
                        .Id = 5 '21.00%
                        .BaseImp = ImporteNeto05
                        .Importe = ImporteIVA05
                    End With
                    intIndex += 1
                End If
                If ImporteNeto06 <> 0 Then
                    ArrayOfIVA(intIndex) = New wsfev1.AlicIva
                    With ArrayOfIVA(intIndex)
                        .Id = 6 '27.00%
                        .BaseImp = ImporteNeto06
                        .Importe = ImporteIVA06
                    End With
                    intIndex += 1
                End If
                .Iva = ArrayOfIVA
            End If
        End With
        objFECAERequest.FeDetReq = ArrayOfFECAEDetRequest

        ' Invoco al m�todo FEAutRequest
        Try
            LogToFile(Now & " Call FECAESolicitar")
            objFECAEResponse = objWSFEv1.FECAESolicitar(objFEAuthRequest, objFECAERequest)
            LogToFile(Now & " Return from FECAESolicitar")
            If Not objFECAEResponse.Errors Is Nothing Then
                LogToFile("FECAEResponse " & objFECAEResponse.Errors.Length & " Errors Found.")
                outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf &
                                      "Archivo:" & FileName & vbCrLf
                For Each e As wsfev1.Err In objFECAEResponse.Errors()
                    outErrorDescription = outErrorDescription & "Error: " & e.Code & "; " & e.Msg & vbCrLf
                    OutData.ErrorCode = e.Code
                Next
                outCAE = ""
                outVencimiento = ""
                outReferencia = ""
            ElseIf objFECAEResponse.FeCabResp Is Nothing Then
                LogToFile("FECAEResponse FeCabResp Is Nothing.")
                outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf &
                                      "Archivo:" & FileName & vbCrLf &
                                      "Detalle del Error: No hubo Responesta de AFIP."
                outCAE = ""
                outVencimiento = ""
                outReferencia = ""
                OutData.ErrorCode = -4
            Else
                LogToFile(Now & vbCrLf &
                "With FECAEResponse.FeCabResp" & vbCrLf &
                "....CantReg   : " & objFECAEResponse.FeCabResp.CantReg & vbCrLf &
                "....PtoVta    : " & objFECAEResponse.FeCabResp.PtoVta & vbCrLf &
                "....CbteTipo  : " & objFECAEResponse.FeCabResp.CbteTipo & vbCrLf &
                "....FchProceso: " & objFECAEResponse.FeCabResp.FchProceso & vbCrLf &
                "....Resultado : " & objFECAEResponse.FeCabResp.Resultado & vbCrLf &
                "End With")
                Dim intI As Integer = 0
                If objFECAEResponse.FeCabResp.Resultado = "R" Then
                    outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf &
                                          "Archivo:" & FileName & vbCrLf &
                                          "Detalle del Error: Encabezado de solicitud rechazado por AFIP." & vbCrLf &
                                          "Motivos del Rechazo: " & vbCrLf
                    If Not objFECAEResponse.Errors Is Nothing Then
                        For Each e As wsfev1.Err In objFECAEResponse.Errors
                            LogToFile("FECAEResponse.Errors(" & intI & ").Code: " & e.Code & "; .Msg: " & e.Msg)
                            outErrorDescription = outErrorDescription & "(" & e.Code & ") " & e.Msg & vbCrLf
                            OutData.ErrorCode = e.Code
                            OutData.ErrorDesc = e.Msg
                        Next
                    Else
                        For Each o As wsfev1.Obs In objFECAEResponse.FeDetResp(0).Observaciones
                            LogToFile("FECAEResponse.FeDetResp(0).Observaciones(" & intI & ").Code: " & o.Code & "; .Msg: " & o.Msg)
                            outErrorDescription = outErrorDescription & "(" & o.Code & ") " & o.Msg & vbCrLf
                            OutData.ErrorCode = o.Code
                            OutData.ErrorDesc = o.Msg
                        Next
                    End If
                    outCAE = ""
                    outVencimiento = ""
                    outReferencia = ""
                ElseIf objFECAEResponse.FeDetResp(0).Resultado = "R" Then
                    outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf &
                                          "Archivo:" & FileName & vbCrLf &
                                          "Detalle del Error: Detalle de solicitud rechazado por AFIP." & vbCrLf &
                                          "Motivos del Rechazo: " & vbCrLf
                    For Each o As wsfev1.Obs In objFECAEResponse.FeDetResp(0).Observaciones
                        LogToFile("FECAEResponse.FeDetResp(0).Observaciones(" & intI & ").Code: " & o.Code & "; .Msg: " & o.Msg)
                        outErrorDescription = outErrorDescription & "(" & o.Code & ") " & o.Msg & vbCrLf
                        OutData.ErrorCode = o.Code
                        OutData.ErrorDesc = o.Msg
                    Next
                    outCAE = ""
                    outVencimiento = ""
                    outReferencia = ""
                Else
                    outCAE = objFECAEResponse.FeDetResp(0).CAE.ToString & ""
                    outVencimiento = objFECAEResponse.FeDetResp(0).CAEFchVto & "" '01/01/2001
                    outReferencia = Format$(objFECAEResponse.FeCabResp.CbteTipo, "00") & "-" &
                                    Format$(objFECAEResponse.FeCabResp.PtoVta, "0000") & "-" &
                                    Format$(objFECAEResponse.FeDetResp(0).CbteDesde, "00000000")
                    SolicitarCAE = True
                End If
                'Next d
            End If
        Catch ex As TimeoutException
            LogToFile("Error en mdlWSFEv1.ObtenerProximoID: " & ex.Message)
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf &
                               "El servidor de AFIP no ha respondido en el tiempo esperado." & vbCrLf &
                               "Consultar en Mesa ayuda sri@afip.gov.ar o 0800-333-6372 opci�n 1 si el servidor de AFIP se encuentra en l�nea."
            OutData.ErrorCode = "timeout"
            SolicitarCAE = False
        Catch ex As Exception
            LogToFile(Now & vbCrLf & "Error en mdlWSFEv1.SolicitarCAE: " & ex.Message)
            outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf &
                                  "Archivo:" & FileName & vbCrLf &
                                  "Detalle del Error: " & vbCrLf &
                                  ex.Message
            OutData.ErrorCode = ex.Message
            SolicitarCAE = False
        End Try

        Dim intSufijo As Integer = 1
        Do Until File.Exists(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FECAERequest" & Format$(intSufijo, "00000000") & ".xml") = False
            intSufijo += 1
        Loop
        ' Obtengo los XML de request/response y los escribo en el disco
        Try
            Dim writer1 As New XmlSerializer(GetType(wsfev1.FECAERequest))
            Dim file1 As New StreamWriter(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FECAERequest" & Format$(intSufijo, "00000000") & ".xml")
            writer1.Serialize(file1, objFECAERequest)
            file1.Close()
        Catch ex As Exception
            LogToFile(Now & " SolicitarCAE.Error: " & ex.Message)
        End Try
        Try
            Dim writer2 As New XmlSerializer(GetType(wsfev1.FECAEResponse))
            Dim file2 As New StreamWriter(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FECAEResponse" & Format$(intSufijo, "00000000") & ".xml")
            writer2.Serialize(file2, objFECAEResponse)
            file2.Close()
        Catch ex As Exception
            LogToFile(Now & " SolicitarCAE.Error: " & ex.Message)
        End Try
    End Function

    Public Function SolicitarCAEenLote(ByVal detalle() As SolicitudCAEDetalle, ByVal outErrorDescription() As String) As Boolean
        Try
            Dim intTipoComprobante As Integer() = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 39, 40, 60, 61, 63, 64}
            Dim intTC As Integer
            Dim intDet As Integer

            If mdlWSFEv1.CUIT = vbNullString Then
                LogToFile("mdlWSFEv1.SolicitarCAE() CUIT no inicializado.")
                detalle(0).outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & "Archivo:" & detalle(0).FileName & vbCrLf & "Detalle del Error: CUIT no inicializado."
                Return False
            End If
            If mdlWSFEv1.Token = vbNullString Then
                LogToFile("mdlWSFEv1.SolicitarCAE() Token no inicializado.")
                detalle(0).outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & "Archivo:" & detalle(0).FileName & vbCrLf & "Detalle del Error: Token no inicializado."
                Return False
            End If
            If mdlWSFEv1.Sign = vbNullString Then
                LogToFile("mdlWSFEv1.SolicitarCAE() Sign no inicializado.")
                detalle(0).outErrorDescription = "Error al Solicitar CAE en AFIP." & vbCrLf & "Archivo:" & detalle(0).FileName & vbCrLf & "Detalle del Error: Sign no inicializado."
                Return False
            End If

            objFEAuthRequest.Cuit = mdlWSFEv1.CUIT
            objFEAuthRequest.Token = mdlWSFEv1.Token
            objFEAuthRequest.Sign = mdlWSFEv1.Sign

            Dim header(15) As wsfev1.FECAECabRequest
            Dim ultnum(15) As Integer

            For intTC = 0 To 15
                header(intTC) = New wsfev1.FECAECabRequest
                If AppConfig.RenumerarSegunAfip = 1 Then
                    Dim intNextNum As Integer = mdlWSFEv1.ObtenerProximoNumeroComprobante(intTipoComprobante(intTC), InData.PuntoDeVenta, "dummy.txt")
                    ultnum(intTC) = intNextNum
                Else
                    ultnum(intTC) = 0
                End If

                With header(intTC)
                    .CantReg = 0
                    .CbteTipo = intTipoComprobante(intTC)
                    .PtoVta = detalle(0).PuntoDeVenta 'estoy suponiendo que hay solicitudes para un solo punto de venta
                End With

                'cuento la cantidad de comprobantes del mismo tipo
                For intDet = 0 To detalle.Length - 1
                    If header(intTC).CbteTipo = detalle(intDet).TipoComprobante Then
                        header(intTC).CantReg += 1
                    End If
                Next
            Next

            Dim intCnt As Integer
            Dim intMax As Integer = ConsultarCantidadMaximaPorPedido(objFEAuthRequest)
            Dim Detail() As wsfev1.FECAEDetRequest
            For intTC = 0 To 15
                If header(intTC).CantReg > 0 Then
                    Dim pedido As New wsfev1.FECAERequest
                    Dim intCurrentMax As Integer
                    If header(intTC).CantReg <intMax Then
                        intCurrentMax= header(intTC).CantReg
                    Else
                        intCurrentMax= intMax
                    End If
                    ReDim Detail(intCurrentMax - 1)
                    intCnt = 0
                    For intDet = 0 To detalle.Length - 1
                        'Detalles del comprobante
                        If header(intTC).CbteTipo = detalle(intDet).TipoComprobante Then
                            Detail(intCnt) = New wsfev1.FECAEDetRequest
                            With Detail(intCnt) 'validar todo
                                .Concepto = 1 'PRODUCTOS
                                .DocTipo = detalle(intDet).TipoDoc
                                .DocNro = detalle(intDet).NroDoc
                                Dim OffsetNumeracion As Integer
                                OffsetNumeracion = (detalle(intDet).NumeroDesde - ultnum(intTC)) * -1
                                .CbteDesde = detalle(intDet).NumeroDesde + OffsetNumeracion
                                .CbteHasta = detalle(intDet).NumeroHasta + OffsetNumeracion
                                LogToFile("Renumerando comprobante de: " & detalle(intDet).NumeroDesde & " a " & .CbteDesde)
                                LogToFile("Renumerando comprobante de: " & detalle(intDet).NumeroHasta & " a " & .CbteHasta)
                                ultnum(intTC) += (detalle(intDet).NumeroHasta - detalle(intDet).NumeroDesde + 1)
                                detalle(intDet).FechaComprobante = Date.Today
                                .CbteFch = Format$(detalle(intDet).FechaComprobante, "yyyyMMdd")
                                .ImpTotal = detalle(intDet).ImporteTotal
                                .ImpTotConc = detalle(intDet).ImporteNoGravado 'No Gravado
                                .ImpNeto = detalle(intDet).ImporteNeto
                                .ImpOpEx = detalle(intDet).ImporteEXE
                                .ImpTrib = detalle(intDet).ImporteTributo01 + detalle(intDet).ImporteTributo02 + detalle(intDet).ImporteTributo03 + detalle(intDet).ImporteTributo04 + detalle(intDet).ImporteTributo99 'Perc. II BB
                                .ImpIVA = detalle(intDet).ImporteIVA
                                If .Concepto = 2 Or .Concepto = 3 Then
                                    .FchServDesde = Format$(detalle(intDet).FechaServDesde, "yyyyMMdd")
                                    .FchServHasta = Format$(detalle(intDet).FechaServHasta, "yyyyMMdd")
                                    .FchVtoPago = Format$(detalle(intDet).FechaVencimiento, "yyyyMMdd")
                                End If
                                .MonId = "PES"
                                .MonCotiz = 1

                                '--- TRIBUTOS
                                Dim intCantidadTributos As Integer = 0
                                If detalle(intDet).BaseImponibleTributo01 <> 0 Then intCantidadTributos += 1
                                If detalle(intDet).BaseImponibleTributo02 <> 0 Then intCantidadTributos += 1
                                If detalle(intDet).BaseImponibleTributo03 <> 0 Then intCantidadTributos += 1
                                If detalle(intDet).BaseImponibleTributo04 <> 0 Then intCantidadTributos += 1
                                If detalle(intDet).BaseImponibleTributo99 <> 0 Then intCantidadTributos += 1
                                If intCantidadTributos > 0 Then
                                    Dim ArrayOfTributos(intCantidadTributos - 1) As wsfev1.Tributo
                                    Dim intTIndex As Integer = 0
                                    If detalle(intDet).BaseImponibleTributo01 <> 0 Then
                                        ArrayOfTributos(intTIndex) = New wsfev1.Tributo
                                        With ArrayOfTributos(intTIndex)
                                            .Id = 1 'IMPUESTOS NACIONALES
                                            .Desc = "IMPUESTOS NACIONALES"
                                            .BaseImp = detalle(intDet).BaseImponibleTributo01
                                            .Alic = detalle(intDet).AlicuotaTributo01
                                            .Importe = detalle(intDet).ImporteTributo01
                                        End With
                                        intTIndex += 1
                                    End If
                                    If detalle(intDet).BaseImponibleTributo02 <> 0 Then
                                        ArrayOfTributos(intTIndex) = New wsfev1.Tributo
                                        With ArrayOfTributos(intTIndex)
                                            .Id = 2 'IMPUESTOS PROVINCIALES
                                            .Desc = "Percepcion Ingresos Brutos Buenos Aires"
                                            .BaseImp = detalle(intDet).BaseImponibleTributo02
                                            .Alic = detalle(intDet).AlicuotaTributo02
                                            .Importe = detalle(intDet).ImporteTributo02
                                        End With
                                        intTIndex += 1
                                    End If
                                    If detalle(intDet).BaseImponibleTributo03 <> 0 Then
                                        ArrayOfTributos(intTIndex) = New wsfev1.Tributo
                                        With ArrayOfTributos(intTIndex)
                                            .Id = 3 'IMPUESTOS MUNICIPALES
                                            .Desc = "IMPUESTOS MUNICIPALES"
                                            .BaseImp = detalle(intDet).BaseImponibleTributo03
                                            .Alic = detalle(intDet).AlicuotaTributo03
                                            .Importe = detalle(intDet).ImporteTributo03
                                        End With
                                        intTIndex += 1
                                    End If
                                    If detalle(intDet).BaseImponibleTributo04 <> 0 Then
                                        ArrayOfTributos(intTIndex) = New wsfev1.Tributo
                                        With ArrayOfTributos(intTIndex)
                                            .Id = 4 'IMPUESTOS INTERNOS
                                            .Desc = "IMPUESTOS INTERNOS"
                                            .BaseImp = detalle(intDet).BaseImponibleTributo04
                                            .Alic = detalle(intDet).AlicuotaTributo04
                                            .Importe = detalle(intDet).ImporteTributo04
                                        End With
                                        intTIndex += 1
                                    End If
                                    If detalle(intDet).BaseImponibleTributo99 <> 0 Then
                                        ArrayOfTributos(intTIndex) = New wsfev1.Tributo
                                        With ArrayOfTributos(intTIndex)
                                            .Id = 99 'IMPUESTOS VARIOS
                                            .Desc = "IMPUESTOS VARIOS"
                                            .BaseImp = detalle(intDet).BaseImponibleTributo99
                                            .Alic = detalle(intDet).AlicuotaTributo99
                                            .Importe = detalle(intDet).ImporteTributo99
                                        End With
                                        intTIndex += 1
                                    End If
                                    .Tributos = ArrayOfTributos
                                End If

                                '--- IVA
                                Dim intCantidadAlicuotas As Integer = 0
                                Dim blnForzar As Boolean = False
                                If detalle(intDet).ImporteNeto03 <> 0 Then
                                    intCantidadAlicuotas += 1
                                End If
                                If detalle(intDet).ImporteNeto04 <> 0 Then
                                    intCantidadAlicuotas += 1
                                End If
                                If detalle(intDet).ImporteNeto05 <> 0 Then
                                    intCantidadAlicuotas += 1
                                End If
                                If detalle(intDet).ImporteNeto06 <> 0 Then
                                    intCantidadAlicuotas += 1
                                End If
                                'If intCantidadAlicuotas = 0 And ImporteTotal > 0 Then
                                '    intCantidadAlicuotas = 1
                                '    blnForzar = True
                                'End If
                                If intCantidadAlicuotas > 0 Then
                                    Dim ArrayOfIVA(intCantidadAlicuotas - 1) As wsfev1.AlicIva
                                    '3	 0.00%
                                    '4	10.50%
                                    '5	21.00%
                                    '6	27.00%
                                    Dim intIndex As Integer = 0
                                    If detalle(intDet).ImporteNeto03 <> 0 Or blnForzar Then
                                        ArrayOfIVA(intIndex) = New wsfev1.AlicIva
                                        With ArrayOfIVA(intIndex)
                                            .Id = 3 '0.00%
                                            .BaseImp = detalle(intDet).ImporteNeto03
                                            .Importe = detalle(intDet).ImporteIVA03
                                        End With
                                        intIndex += 1
                                    End If
                                    If detalle(intDet).ImporteNeto04 <> 0 Then
                                        ArrayOfIVA(intIndex) = New wsfev1.AlicIva
                                        With ArrayOfIVA(intIndex)
                                            .Id = 4 '10.50%
                                            .BaseImp = detalle(intDet).ImporteNeto04
                                            .Importe = detalle(intDet).ImporteIVA04
                                        End With
                                        intIndex += 1
                                    End If
                                    If detalle(intDet).ImporteNeto05 <> 0 Then
                                        ArrayOfIVA(intIndex) = New wsfev1.AlicIva
                                        With ArrayOfIVA(intIndex)
                                            .Id = 5 '21.00%
                                            .BaseImp = detalle(intDet).ImporteNeto05
                                            .Importe = detalle(intDet).ImporteIVA05
                                        End With
                                        intIndex += 1
                                    End If
                                    If detalle(intDet).ImporteNeto06 <> 0 Then
                                        ArrayOfIVA(intIndex) = New wsfev1.AlicIva
                                        With ArrayOfIVA(intIndex)
                                            .Id = 6 '27.00%
                                            .BaseImp = detalle(intDet).ImporteNeto06
                                            .Importe = detalle(intDet).ImporteIVA06
                                        End With
                                        intIndex += 1
                                    End If
                                    .Iva = ArrayOfIVA
                                End If
                            End With
                            intCnt += 1
                            If intCnt >= intMax Then
                                'pedir ahora
                                header(intTC).CantReg = intCnt
                                intCnt = 0
                                If Not Pedir(header, pedido, intTC, Detail, outErrorDescription) _
                                   Then
                                    LogToFileAndScreen(outErrorDescription(intTC), "")
                                    Exit Function
                                End If
                            End If
                        End If
                    Next intDet
                    If intCnt > 0 _
                       Then
                        If Not Pedir(header, pedido, intTC, Detail, outErrorDescription) _
                           Then
                            LogToFileAndScreen(outErrorDescription(intTC), "")
                            Exit Function
                        End If
                    End If
                End If
            Next intTC
        Catch ex As Exception
            LogToFileAndScreen(Now & " " & ex.Message, "")
        End Try
    End Function

    Private Function Pedir( _
        ByVal header() As wsfev1.FECAECabRequest, _
        ByVal pedido As wsfev1.FECAERequest, _
        ByVal intTC As Integer, _
        ByVal Detail() As wsfev1.FECAEDetRequest, _
        ByVal outErrorDescription() As String)

        Pedir = True

        pedido.FeCabReq = header(intTC)
        pedido.FeDetReq = Detail

        Dim respuesta As New wsfev1.FECAEResponse
        ' Invoco al m�todo FEAutRequest
        Try
            LogToFile(Now & " Call FECAESolicitar")
            respuesta = objWSFEv1.FECAESolicitar(objFEAuthRequest, pedido)
            LogToFile(Now & " Return from FECAESolicitar")
            If respuesta.FeCabResp Is Nothing Then
                LogToFile("FECAEResponse FeCabResp Is Nothing.")
                outErrorDescription(intTC) = "Error al Solicitar CAE en AFIP." & vbCrLf & "Detalle del Error: No hubo Responesta de AFIP."
                Pedir = False
            Else
                LogToFile(Now & vbCrLf & _
                "With FECAEResponse.FeCabResp" & vbCrLf & _
                "....CantReg   : " & respuesta.FeCabResp.CantReg & vbCrLf & _
                "....PtoVta    : " & respuesta.FeCabResp.PtoVta & vbCrLf & _
                "....CbteTipo  : " & respuesta.FeCabResp.CbteTipo & vbCrLf & _
                "....FchProceso: " & respuesta.FeCabResp.FchProceso & vbCrLf & _
                "....Resultado : " & respuesta.FeCabResp.Resultado & vbCrLf & _
                "End With")
                Dim intI As Integer = 0
                If respuesta.FeCabResp.Resultado = "R" Or _
                   respuesta.FeCabResp.Resultado = "P" Then
                    outErrorDescription(intTC) = _
                        "Error al Solicitar CAE en AFIP." & vbCrLf & _
                        "Detalle del Error: Encabezado de solicitud rechazado por AFIP." & vbCrLf & _
                        "Motivos del Rechazo: " & vbCrLf
                    If Not respuesta.Errors Is Nothing Then
                        For Each e As wsfev1.Err In respuesta.Errors
                            LogToFile("FECAEResponse.Errors(" & intI & ").Code: " & e.Code & "; .Msg: " & e.Msg)
                            outErrorDescription(intTC) = outErrorDescription(intTC) & "(" & e.Code & ") " & e.Msg & vbCrLf
                        Next
                    Else
                        If respuesta.FeCabResp.Resultado = "P" Then '1 factura dentro del detalle tir� error...
                            For i As Integer = 0 To respuesta.FeDetResp.Length - 1
                                If Not respuesta.FeDetResp(i).Observaciones Is Nothing Then
                                    For Each o As wsfev1.Obs In respuesta.FeDetResp(i).Observaciones
                                        LogToFile("FECAEResponse.FeDetResp(" & i & ").Observaciones(" & intI & ").Code: " & o.Code & "; .Msg: " & o.Msg)
                                        outErrorDescription(intTC) = outErrorDescription(intTC) & "Com: " & respuesta.FeDetResp(i).CbteDesde & " (" & o.Code & ") " & o.Msg & vbCrLf
                                    Next
                                End If
                            Next
                        End If
                    End If
                    Pedir = False
                ElseIf respuesta.FeDetResp(0).Resultado = "R" Then
                    Pedir = False
                    outErrorDescription(intTC) = "Error al Solicitar CAE en AFIP." & vbCrLf & "Detalle del Error: Detalle de solicitud rechazado por AFIP." & vbCrLf & "Motivos del Rechazo: " & vbCrLf
                    For Each o As wsfev1.Obs In respuesta.FeDetResp(0).Observaciones
                        LogToFile("FECAEResponse.FeDetResp(0).Observaciones(" & intI & ").Code: " & o.Code & "; .Msg: " & o.Msg)
                        outErrorDescription(intTC) = outErrorDescription(intTC) & "(" & o.Code & ") " & o.Msg & vbCrLf
                    Next
                Else
                    'outCAE = respuesta.FeDetResp(0).CAE.ToString & ""
                    'outVencimiento = respuesta.FeDetResp(0).CAEFchVto & "" '01/01/2001
                    'outReferencia = Format$(respuesta.FeCabResp.CbteTipo, "00") & "-" & _
                    '                Format$(respuesta.FeCabResp.PtoVta, "0000") & "-" & _
                    ' Format$(respuesta.FeDetResp(0).CbteHasta, "00000000")
                End If
            End If
        Catch ex As TimeoutException
            LogToFile("Error en mdlWSFEv1.ObtenerProximoID: " & ex.Message)
            outErrorDescription(intTC) = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                                         "El servidor de AFIP no ha respondido en el tiempo esperado." & vbCrLf & _
                                         "Consultar en Mesa ayuda sri@afip.gov.ar o 0800-333-6372 opci�n 1 si el servidor de AFIP se encuentra en l�nea."
            Pedir = False
        Catch ex As Exception
            LogToFile(Now & vbCrLf & "Error en mdlWSFEv1.SolicitarCAE: " & ex.Message)
            outErrorDescription(intTC) = "Error al Solicitar CAE en AFIP." & vbCrLf & _
                                         "Detalle del Error: " & vbCrLf & _
                                         ex.Message
            Pedir = False
        End Try

        Dim intSufijo As Integer = 1
        Do Until File.Exists(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FECAERequest" & Format$(intSufijo, "00000000") & ".xml") = False
            intSufijo += 1
        Loop
        ' Obtengo los XML de request/response y los escribo en el disco
        Try
            Dim writer1 As New XmlSerializer(GetType(wsfev1.FECAERequest))
            Dim file1 As New StreamWriter(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FECAERequest" & Format$(intSufijo, "00000000") & ".xml")
            writer1.Serialize(file1, pedido)
            file1.Close()
        Catch ex As Exception
            LogToFile(Now & " Pedir.Error: " & ex.Message)
        End Try
        Try
            Dim writer2 As New XmlSerializer(GetType(wsfev1.FECAEResponse))
            Dim file2 As New StreamWriter(My.Application.Info.DirectoryPath & "\RequestLog\wsfe_FECAEResponse" & Format$(intSufijo, "00000000") & ".xml")
            writer2.Serialize(file2, respuesta)
            file2.Close()
        Catch ex As Exception
            LogToFile(Now & " Pedir.Error: " & ex.Message)
        End Try
    End Function

    Public Function ConsultarTributos() As String
        Dim strRet As String = ""
        Try
            Dim rta As wsfev1.FETributoResponse = objWSFEv1.FEParamGetTiposTributos(objFEAuthRequest)
            If Not rta Is Nothing Then
                For Each t As wsfev1.TributoTipo In rta.ResultGet
                    strRet = strRet & t.Id & vbTab & t.Desc & vbCrLf
                Next
            End If
        Catch ex As Exception
            LogToFile(Now & " ConstultarTributos.Error: " & ex.Message)
        End Try
        Return strRet
    End Function

    Public Function ConsultarTiposDeIVA() As String
        Dim strRet As String = ""
        Try
            Dim rta As wsfev1.IvaTipoResponse = objWSFEv1.FEParamGetTiposIva(objFEAuthRequest)
            If Not rta Is Nothing Then
                For Each t As wsfev1.IvaTipo In rta.ResultGet
                    strRet = strRet & t.Id & vbTab & t.Desc & vbCrLf
                Next
            End If
        Catch ex As Exception
            LogToFile(Now & " ConstultarTiposDeIVA.Error: " & ex.Message)
        End Try
        Return strRet
    End Function

    Public Function ConsultarDocTipo() As String
        Dim strRet As String = ""
        Try
            Dim rta As wsfev1.DocTipoResponse = objWSFEv1.FEParamGetTiposDoc(objFEAuthRequest)
            If Not rta Is Nothing Then
                For Each t As wsfev1.DocTipo In rta.ResultGet
                    strRet = strRet & t.Id & vbTab & t.Desc & vbCrLf
                Next
            End If
        Catch ex As Exception
            LogToFile(Now & " ConstultarDocTipo.Error: " & ex.Message)
        End Try
        Return strRet
    End Function

    Public Function ConsultarCantidadMaximaPorPedido(ByVal Auth As wsfev1.FEAuthRequest) As Integer
        Dim strRet As String = ""
        Try
            Dim rta As wsfev1.FERegXReqResponse = objWSFEv1.FECompTotXRequest(Auth)
            If Not rta Is Nothing Then
                Return rta.RegXReq & vbCrLf
            End If
        Catch ex As Exception
            LogToFile(Now & " ConstultarDocTipo.Error: " & ex.Message)
        End Try
        Return 0
    End Function


    Public Function ConsultarCAE(ByVal TipoComprobante As Integer, ByVal PuntoDeVenta As Integer, ByVal NumeroComprobante As Integer) As String
        'TIPO COMPROBANTE:
        'FV-A=01
        'FV-B-06
        If Inicializado() = False Then
            Dim strErrorDescription As String = ""
            If AutenticacionWSAA.ObtenerAutorizacion(strErrorDescription) = True Then
                Inicializar(AppConfig.CUIT) 'Para refrescar el Token y Sign
                If Inicializado() = False Then
                    LogToFile(Now & " Token y Sign no inicializados... seguramente habra un error en ObtenerProximoNumeroComprobante")
                End If
            Else
                LogToFile(Now & strErrorDescription)
            End If
        End If
        objFEAuthRequest.Cuit = mdlWSFEv1.CUIT 'validar
        objFEAuthRequest.Token = mdlWSFEv1.Token 'validar
        objFEAuthRequest.Sign = mdlWSFEv1.Sign 'validar

        Try
            Dim strRet As String = vbNullString
            Dim FeCompConsReg As wsfev1.FECompConsultaReq = New wsfev1.FECompConsultaReq
            With FeCompConsReg
                .CbteTipo = TipoComprobante
                .PtoVta = PuntoDeVenta
                .CbteNro = NumeroComprobante
            End With
            Dim rta As wsfev1.FECompConsultaResponse = objWSFEv1.FECompConsultar(objFEAuthRequest, FeCompConsReg)
            If rta.Errors Is Nothing Then
                strRet = "" & _
                        "Resultado: " & rta.ResultGet().Resultado & vbCrLf & _
                        "Tipo de Comprobante: " & rta.ResultGet().CbteTipo & vbCrLf & _
                        "N�mero: " & Format$(rta.ResultGet().PtoVta, "0000") & "-" & Format$(rta.ResultGet().CbteDesde, "00000000") & " A " & Format$(rta.ResultGet().CbteHasta, "00000000") & vbCrLf & _
                        "Fecha: " & rta.ResultGet().CbteFch & vbCrLf & _
                        "Doc.Tipo: " & rta.ResultGet().DocTipo & vbCrLf & _
                        "Doc.N�mero: " & rta.ResultGet().DocNro & vbCrLf & _
                        "Importe Neto: " & rta.ResultGet().ImpNeto & vbCrLf & _
                        "Importe IVA: " & rta.ResultGet().ImpIVA & vbCrLf & _
                        "Importe Op.Ex.: " & rta.ResultGet().ImpOpEx & vbCrLf & _
                        "Importe Total: " & rta.ResultGet().ImpTotal & vbCrLf & _
                        "Importe Total Conc.: " & rta.ResultGet().ImpTotConc & vbCrLf & _
                        "Importe Tributos: " & rta.ResultGet().ImpTrib & vbCrLf & _
                        "CAE: " & rta.ResultGet().CodAutorizacion & vbCrLf & _
                        "Vencimiento CAE: " & rta.ResultGet().FchVto & vbCrLf & _
                        "Concepto: " & rta.ResultGet().Concepto & vbCrLf & _
                        "Tipo Emisi�n: " & rta.ResultGet().EmisionTipo & vbCrLf & _
                        "Fecha de Proceso: " & rta.ResultGet().FchProceso & vbCrLf & _
                        "Fecha Svc. Desde: " & rta.ResultGet().FchServDesde & vbCrLf & _
                        "Fecha Svc. Hasta: " & rta.ResultGet().FchServHasta & vbCrLf & _
                        "Vencimiento Pago: " & rta.ResultGet().FchVtoPago & vbCrLf & _
                        "Moneda: " & rta.ResultGet().MonId & vbCrLf & _
                        "Cotizaci�n: " & rta.ResultGet().MonCotiz & vbCrLf

                If Not rta.ResultGet().Iva Is Nothing Then
                    strRet &= "Detalle IVA:" & rta.ResultGet().Iva.Length & vbCrLf
                    For Each ai As wsfev1.AlicIva In rta.ResultGet().Iva
                        strRet &= "...(" & ai.Id & "): Base Imponible: " & ai.BaseImp & "; Importe: " & ai.Importe & vbCrLf
                    Next
                End If
                If Not rta.ResultGet().Observaciones Is Nothing Then
                    strRet &= "Observaciones:" & rta.ResultGet().Observaciones.Length & vbCrLf
                    For Each ob As wsfev1.Obs In rta.ResultGet().Observaciones
                        strRet &= "...(" & ob.Code & "): " & ob.Msg & vbCrLf
                    Next
                End If
                If Not rta.ResultGet().Opcionales Is Nothing Then
                    strRet &= "Opcionales: " & rta.ResultGet().Opcionales.Length & vbCrLf
                    For Each op As wsfev1.Opcional In rta.ResultGet().Opcionales
                        strRet &= "...(" & op.Id & "): " & op.Valor & vbCrLf
                    Next
                End If
                If Not rta.ResultGet().CbtesAsoc Is Nothing Then
                    strRet &= "Comprobantes Asociados: " & rta.ResultGet().CbtesAsoc.Length & vbCrLf
                    For Each ca As wsfev1.CbteAsoc In rta.ResultGet().CbtesAsoc
                        strRet &= "...Tipo: " & ca.Tipo & "; Nro.: " & Format$(ca.PtoVta, "0000") & "-" & Format$(ca.Nro, "00000000") & vbCrLf
                    Next
                End If
                If Not rta.ResultGet().Tributos Is Nothing Then
                    strRet &= "Tributos:" & rta.ResultGet().Tributos.Length & vbCrLf
                    For Each tr As wsfev1.Tributo In rta.ResultGet().Tributos
                        strRet &= "...(" & tr.Id & ") Alic:" & tr.Alic & "; Base Imp.: " & tr.BaseImp & "; Desc: " & tr.Desc & "; Importe: " & tr.Importe & vbCrLf
                    Next
                End If
                Return strRet
            Else
                strRet = "" & _
                "Error: " & vbCrLf
                Dim e As Integer
                For e = 0 To rta.Errors.Length - 1
                    strRet = strRet & "[" & rta.Errors(e).Code & "] " & rta.Errors(e).Msg & vbCrLf
                Next
                Return strRet
            End If
        Catch ex As Exception
            Return "Error: " & ex.Message
        End Try
    End Function

    Public Function CallDummy() As String

        Try
            Dim rta As wsfev1.DummyResponse = objWSFEv1.FEDummy()
            If Not rta Is Nothing Then
                Return "AppServer: " & rta.AppServer & vbCrLf & _
                       "AuthServer: " & rta.AuthServer & vbCrLf & _
                       "DbServer: " & rta.DbServer
            End If
        Catch ex As Exception
            LogToFile(Now & " CallDummy.Error: " & ex.Message)
        End Try
        Return 0
    End Function

    Public Sub ResetToken()
        LogToFile(Now & " mdlWSFEv1.ResetToken()")
        Token = ""
    End Sub

    Public Function ConsultarTiposComprobante() As String
        Return ""
    End Function

End Module
